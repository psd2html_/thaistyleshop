<?php
class ControllerPaymentRbs extends Controller {
    /**
     * Инициализация языкового пакета
     * @param $registry
     */
    public function __construct($registry) {
        parent::__construct($registry);
        $this->load->language('payment/rbs');
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/rbs.tpl')) {
            $this->have_template = true;
        }
    }

    /**
     * Рендеринг кнопки-ссылки для перехода в метод payment()
     * @return mixed Шаблон кнопки
     */
    public function index() {
        $data['action'] = $this->url->link('payment/rbs/payment');
        $data['continue'] = $this->url->link('payment/rbs/payment');
        $data['button_confirm'] = $this->language->get('button_confirm');
        return $this->get_template('payment/rbs', $data);
    }

    /**
     * Регистрация заказа.
     * Переадресация покупателя при успешной регистрации.
     * Вывод ошибки при неуспешной регистрации.
     */
    public function payment() {
        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            //return;
        }

        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        $order_number = $this->session->data['order_id'];
        $amount = (int) $order_info['total'] * 100;
        $return_url = $this->url->link('payment/rbs/callback');

        $this->initializeRbs();
        $response = $this->rbs->register_order($order_number, $amount, $return_url);

        if (isset($response['errorCode'])) {
            $this->document->setTitle($this->language->get('error_title'));

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['button_continue'] = $this->language->get('error_continue');

            $data['heading_title'] = $this->language->get('error_title') . ' #' . $response['errorCode'];
            $data['text_error'] = $response['errorMessage'];
            $data['continue'] = $this->url->link('checkout/cart');

            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->get_template('error/rbs', $data));
        } else {
            $this->response->redirect($response['formUrl']);
        }
    }

    /**
     * Колбек для возвращения покупателя из ПШ в магазин.
     */
    public function callback() {
        if (isset($this->request->get['orderId'])) {
            $order_id = $this->request->get['orderId'];
        } else {
            die('Illegal Access');
        }

        $this->load->model('checkout/order');
        $order_number = $this->session->data['order_id'];
        $order_info = $this->model_checkout_order->getOrder($order_number);

        if ($order_info) {
            $this->initializeRbs();

            $response = $this->rbs->get_order_status($order_id);
            if(($response['errorCode'] == 0) && (($response['orderStatus'] == 1) || ($response['orderStatus'] == 2))) {
                $this->model_checkout_order->addOrderHistory($order_number, $this->config->get('config_order_status_id'));
                $this->response->redirect($this->url->link('checkout/success', '', true));
            } else {
                $this->response->redirect($this->url->link('checkout/failure', '', true));
            }
        }
    }

    /**
     * Инициализация библиотеки RBS
     */
    private function initializeRbs() {
        $this->library('rbs');
        $this->rbs = new RBS();
        $this->rbs->login = $this->config->get('rbs_merchantLogin');
        $this->rbs->password = $this->config->get('rbs_merchantPassword');
        $this->rbs->stage = $this->config->get('rbs_stage');
        $this->rbs->mode = $this->config->get('rbs_mode');
        $this->rbs->logging = $this->config->get('rbs_logging');
        $this->rbs->currency = $this->config->get('rbs_currency');
    }

    /**
     * В версии 2.1 нет метода Loader::library()
     * Своя реализация
     * @param $library
     */
    private function library($library) {
        $file = DIR_SYSTEM . 'library/' . str_replace('../', '', (string)$library) . '.php';

        if (file_exists($file)) {
            include_once($file);
        } else {
            trigger_error('Error: Could not load library ' . $file . '!');
            exit();
        }
    }

    /**
     * Отрисовка шаблона
     * @param $template     Шаблон вместе с корневой папкой
     * @param $data         Данные
     * @return mixed        Отрисованный шаблон
     */
    private function get_template($template, $data) {
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $template . '.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/' . $template . '.tpl', $data);
        } else {
            return $this->load->view('default/template/' . $template . '.tpl', $data);
        }
    }
    
    
    
    public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'rbs') {
			$this->load->model('checkout/order');
			
			$this->error = array();

			if (!empty($this->request->post['regist']) && ($this->request->post['regist'] == 1)) {
				if ($this->validate() && !$this->customer->isLogged()) {

					$customer_id = $this->model_account_customer->addCustomerReg($this->request->post);

					// Check how many login attempts have been made.
					$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['r_email']);

					// Check if customer has been approved.
					$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['r_email']);

					if (!$this->customer->login($this->request->post['r_email'], $this->request->post['r_password'])) {
						$json['error']['r_name'] = $this->language->get('error_login');

						$this->model_account_customer->addLoginAttempt($this->request->post['email']);
					} else {
						$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

						$this->session->data['account'] = 'register';

						$this->load->model('account/address');

						$adressId = $this->model_account_address->addAddressReg($this->request->post, $customer_id);
						$this->model_account_address->getDefaultAdress($adressId, $customer_id);

						$this->load->model('account/customer_group');

						$customer_group_info = $this->model_account_customer_group->getCustomerGroup($this->config->get('config_customer_group_id'));

						if ($customer_group_info && !$customer_group_info['approval']) {
							$this->customer->login($this->request->post['email'], $this->request->post['password']);

							// Default Payment Address
							$this->load->model('account/address');

							$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());

							if (!empty($adressId)) {
								$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
							}
						}

						// Add to activity log
						$this->load->model('account/activity');

						$activity_data = array(
							'customer_id' => $customer_id,
							'name'        => $this->request->post['r_name']
						);

						$this->model_account_activity->addActivity('register', $activity_data);


						$this->request->post['customer_id'] = $customer_id;
						$this->request->post['customer_group_id'] = 1;
						$this->request->post['payment_firstname'] = $this->request->post['firstname'];
						$this->request->post['payment_address_1'] = '';
						$this->request->post['payment_city'] = $this->request->post['r_sity'];
						$this->request->post['payment_postcode'] = $this->request->post['postcode'];
						$this->request->post['payment_country'] = 'Russian Federation';
						$this->request->post['payment_country_id'] = 176;
						$this->request->post['payment_zone'] = 'Moscow';
						$this->request->post['payment_zone_id'] = 2761;

						$pm = $this->session->data['payment_methods'][$this->request->post['payment_method']];
						$this->request->post['payment_code'] = $this->request->post['payment_method'];
						if (!empty($pm)) {
							$this->request->post['payment_method'] = $pm['title'];
						}

						$this->request->post['shipping_firstname'] = $this->request->post['firstname'];
						$this->request->post['shipping_address_1'] = 'ул. ' . $this->request->post['address_1'] . ', д. ' . $this->request->post['house'] . (!empty($this->request->post['housing']) ? ', корп. ' . $this->request->post['housing'] : '') . (!empty($this->request->post['apartment']) ? ', кв. ' . $this->request->post['apartment'] : '');
						$this->request->post['shipping_city'] = $this->request->post['sity'];
						$this->request->post['shipping_postcode'] = $this->request->post['postcode'];
						$this->request->post['shipping_country'] = 'Russian Federation';
						$this->request->post['shipping_country_id'] = 176;
						$this->request->post['shipping_zone'] = 'Moscow';
						$this->request->post['shipping_zone_id'] = 2761;

						$sma = explode('.',$this->request->post['shipping_method']);
						$this->request->post['shipping_code'] = $this->request->post['shipping_method'];
						if (!empty($sma) && count($sma) == 2) {
							$sm = $this->session->data['shipping_methods'][$sma[0]]['quote'][$sma[1]];
							$this->request->post['shipping_method'] = $sm['title'];
						}

						$this->model_checkout_order->editOrderReg($this->session->data['order_id'],$this->request->post);

						unset($this->session->data['guest']);
						unset($this->session->data['shipping_method']);
						unset($this->session->data['shipping_methods']);
						unset($this->session->data['payment_method']);
						unset($this->session->data['payment_methods']);

						//$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cod_order_status_id'),$this->request->post['comment']);
						
		
						
						$this->error['success'] = $this->url->link('checkout/success');
					}
				}

			} elseif (!empty($this->request->post['regist']) && ($this->request->post['regist'] == 2)) {
				
				//if ($this->customer->isLogged()) {
					
					if ($this->request->post['shipping_address'] == 'new') {
						$valid_adr = $this->validateAdr();
						$this->request->post['address_1'] = 'ул. ' . $this->request->post['address_1'] . ', д. ' . $this->request->post['haus'] . (!empty($this->request->post['korp']) ? ', корп. ' . $this->request->post['korp'] : '') . (!empty($this->request->post['apart']) ? ', кв. ' . $this->request->post['apart'] : '');
						$datad = $this->request->post['address_1'];
						if ($valid_adr) {

						}
					} else {
						$datad = 'ул. ' . $this->session->data['shipping_address']['address_1'] . ', д. ' . $this->session->data['shipping_address']['house'] . (!empty($this->session->data['shipping_address']['housing']) ? ', корп. ' . $this->session->data['shipping_address']['housing'] : '') . (!empty($this->session->data['shipping_address']['apartment']) ? ', кв. ' . $this->session->data['shipping_address']['apartment'] : '');
					}

					if (!empty($this->error)) {
						$json['regist'] = 2;
					} else {

						$this->error['success'] = $this->url->link('checkout/success');
					}
					
					$this->model_checkout_order->updOrder($this->session->data['order_id'], $datad, $this->request->post['comment']);

					//$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cod_order_status_id'),$this->request->post['comment']);
					
				//}
			
			}
			
				$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($this->error));
		}
	}

	public function quick() {

		$this->load->language('checkout/checkout');
		$json = array();
		$this->load->model('checkout/order');

		// q_name
		if ((utf8_strlen(trim($this->request->post['q_name'])) < 1) || (utf8_strlen(trim($this->request->post['q_name'])) > 32)) {
			$json['error']['q_name'] = $this->language->get('error_firstname');
		} else {
			$this->request->post['firstname'] = $this->request->post['q_name'];
		}

		// q_phone
		$sar = array('+', '(', ')', ' ', '-');
		$phone_temp = str_replace($sar, '', $this->request->post['q_phone']);

		if (strlen($phone_temp) != 11 || !is_numeric($phone_temp)) {
			$json['error']['q_phone'] = $this->language->get('error_telephone');
		} else {
			$this->request->post['telephone'] = $this->request->post['q_phone'];
		}

		/*if ((utf8_strlen(trim($this->request->post['q_phone'])) < 3) || (utf8_strlen(trim($this->request->post['q_phone'])) > 32)) {
			$json['error']['q_phone'] = $this->language->get('error_telephone');
		} else {
			$this->request->post['telephone'] = $this->request->post['q_phone'];
		}*/

		// q_postcode
		if ((utf8_strlen(trim($this->request->post['q_postcode'])) < 2) || (utf8_strlen(trim($this->request->post['q_postcode'])) > 10)) {
			$json['error']['q_postcode'] = $this->language->get('error_postcode');
		} else {
			$this->request->post['postcode'] = $this->request->post['q_postcode'];
		}

		// q_sity
		if ((utf8_strlen(trim($this->request->post['q_city'])) < 1) || (utf8_strlen(trim($this->request->post['q_city'])) > 32)) {
			$json['error']['q_city'] = $this->language->get('error_city');
		} else {
			$this->request->post['city'] = $this->request->post['q_city'];
		}

		// q_street
		if ((utf8_strlen(trim($this->request->post['q_street'])) < 3) || (utf8_strlen(trim($this->request->post['q_street'])) > 128)) {
			$json['error']['q_street'] = $this->language->get('error_address_1');
		}

		// q_haus
		if ((utf8_strlen(trim($this->request->post['q_haus'])) < 1) || (utf8_strlen(trim($this->request->post['q_haus'])) > 10)) {
			$json['error']['q_haus'] = $this->language->get('error_haus');
		}

		$this->request->post['address_1'] = 'ул. ' . $this->request->post['q_street'] . ', д. ' . $this->request->post['q_haus'] . (!empty($this->request->post['q_korp']) ? ', корп. ' . $this->request->post['q_korp'] : '') . (!empty($this->request->post['q_apart']) ? ', кв. ' . $this->request->post['q_apart'] : '');

		if (empty($json)) {

			$this->request->post['shipping_firstname'] = $this->request->post['q_name'];
			$this->request->post['shipping_address_1'] = 'ул. ' . $this->request->post['q_street'] . ', д. ' . $this->request->post['q_haus'] . (!empty($this->request->post['q_korp']) ? ', корп. ' . $this->request->post['q_korp'] : '') . (!empty($this->request->post['q_apart']) ? ', кв. ' . $this->request->post['q_apart'] : '');
			$this->request->post['shipping_city'] = $this->request->post['q_city'];
			$this->request->post['shipping_postcode'] = $this->request->post['q_postcode'];
			$this->request->post['shipping_country'] = 'Russian Federation';
			$this->request->post['shipping_country_id'] = 176;
			$this->request->post['shipping_zone'] = 'Moscow';
			$this->request->post['shipping_zone_id'] = 2761;

			$this->request->post['shipping_method'] = 'быстрый заказ';

			$this->model_checkout_order->addOrderGuest($this->session->data['order_id'], $this->config->get('cod_order_status_id'),$this->request->post);
			$json['success'] = $this->url->link('checkout/success');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function validateAdr() {

		$this->load->language('checkout/checkout');
		$json = array();
		// postcode
		if ((utf8_strlen(trim($this->request->post['postcode'])) < 2) || (utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
			$json['error']['postcode'] = $this->language->get('error_postcode');
		}

		// city
		if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			$json['error']['city'] = $this->language->get('error_city');
		}

		// address_1
		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$json['error']['address_1'] = $this->language->get('error_address_1');
		}

		// haus
		if ((utf8_strlen(trim($this->request->post['haus'])) < 1) || (utf8_strlen(trim($this->request->post['haus'])) > 10)) {
			$json['error']['haus'] = $this->language->get('error_haus');
		} else {
			$this->request->post['house'] = $this->request->post['haus'];
		}

		if (!empty($this->request->post['korp'])) {
			$this->request->post['housing'] = $this->request->post['korp'];
		}

		if (!empty($this->request->post['apart'])) {
			$this->request->post['apartment'] = $this->request->post['apart'];
		}

		$out = true;

		if (!empty($json)) {
			$json['regist'] = 2;
			$this->error = $json;

			$out = false;
		}

		return $out;
	}

	public function validate() {

		$this->load->language('checkout/checkout');
		$this->load->model('account/customer');

		$json = array();

		if (isset($this->session->data['account'])) {
			$data['account'] = $this->session->data['account'];
		} else {
			$data['account'] = '';
		}

		if(is_null($this->customer->isLogged()) && ($data['account'] != 'guest')) {

			$this->request->post['store_name'] = $this->config->get('config_name');
			$this->request->post['store_url'] = $this->config->get('config_url');
			// r_name
			if ((utf8_strlen(trim($this->request->post['r_name'])) < 1) || (utf8_strlen(trim($this->request->post['r_name'])) > 32)) {
				$json['error']['r_name'] = $this->language->get('error_firstname');
			} else {
				$this->request->post['firstname'] = $this->request->post['r_name'];
			}

			// r_email:
			if ((utf8_strlen($this->request->post['r_email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['r_email'])) {
				$json['error']['r_email'] = $this->language->get('error_email');
			} elseif ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['r_email'])) {
				$json['error']['r_email'] = $this->language->get('error_exists');
			} else {
				$this->request->post['email'] = $this->request->post['r_email'];
			}

			// r_phone
			$sar = array('+', '(', ')', ' ', '-');
			$phone_temp = str_replace($sar, '', $this->request->post['r_phone']);

			if (strlen($phone_temp) != 11 || !is_numeric($phone_temp)) {
				$json['error']['r_phone'] = $this->language->get('error_telephone');
			} else {
				$this->request->post['telephone'] = $this->request->post['r_phone'];
			}


			/*if ((utf8_strlen(trim($this->request->post['r_phone'])) < 3) || (utf8_strlen(trim($this->request->post['r_phone'])) > 32)) {
				$json['error']['r_phone'] = $this->language->get('error_telephone');
			} else {
				$this->request->post['telephone'] = $this->request->post['r_phone'];
			}*/

			// r_sity
			if ((utf8_strlen(trim($this->request->post['r_sity'])) < 1) || (utf8_strlen(trim($this->request->post['r_sity'])) > 32)) {
				$json['error']['r_sity'] = $this->language->get('error_city');
			}

			// r_password
			if ((utf8_strlen(trim($this->request->post['r_password'])) < 3) || (utf8_strlen(trim($this->request->post['r_password'])) > 20)) {
				$json['error']['r_password'] = $this->language->get('error_password');
			}

			// r_password
			if ($this->request->post['r_password'] != $this->request->post['r_passwordc']) {
				$json['error']['r_password'] = $this->language->get('error_confirm');
			}

			if (!empty($this->request->post['shipping_method']) && $this->request->post['shipping_method'] != 'multiflat.multiflat4') {
				// postcode
				if ((utf8_strlen(trim($this->request->post['postcode'])) < 2) || (utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
					$json['error']['postcode'] = $this->language->get('error_postcode');
				}

				// city
				if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
					$json['error']['city'] = $this->language->get('error_city');
				}

				// address_1
				if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
					$json['error']['address_1'] = $this->language->get('error_address_1');
				}

				// haus
				if ((utf8_strlen(trim($this->request->post['haus'])) < 1) || (utf8_strlen(trim($this->request->post['haus'])) > 10)) {
					$json['error']['haus'] = $this->language->get('error_haus');
				} else {
					$this->request->post['house'] = $this->request->post['haus'];
				}

				if (!empty($this->request->post['korp'])) {
					$this->request->post['housing'] = $this->request->post['korp'];
				}

				if (!empty($this->request->post['apart'])) {
					$this->request->post['apartment'] = $this->request->post['apart'];
				}
			}

		}

		$out = true;

		if (!empty($json)) {
			$json['regist'] = 1;
			$this->error = $json;

			$out = false;
		}

		return $out;
	}
    
    
    
    
    
}