<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - manufacturer_desc.php
 * Date: 05.04.15
 * Time: 15:53
 * Project: thai-style
 */
class ControllerModuleManufacturerDesc extends Controller
{
    public function index()
    {
        $this->load->language('product/manufacturer');
        $this->load->model('catalog/manufacturer');

        $this->load->model('tool/image');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_index'] = $this->language->get('text_index');
        $data['text_empty'] = $this->language->get('text_empty');

        $data['button_continue'] = $this->language->get('button_continue');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_brand'),
            'href' => $this->url->link('product/manufacturer')
        );

        $data['categories'] = array();

        $results = $this->model_catalog_manufacturer->getManufacturers();

        foreach ($results as $result) {
            if (is_numeric(utf8_substr($result['name'], 0, 1))) {
                $key = '0 - 9';
            } else {
                $key = utf8_substr(utf8_strtoupper($result['name']), 0, 1);
            }

            if (!isset($data['categories'][$key])) {
                $data['categories'][$key]['name'] = $key;
            }

            $data['categories'][$key]['manufacturer'][] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
            );
        }

        $data['continue'] = $this->url->link('common/home');

        /*$$data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');*/

        /*if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer_desc.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/manufacturer_desc.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/product/manufacturer_list.tpl', $data));
        }*/
        return $this->load->view($this->config->get('config_template') . '/template/module/manufacturer_desc.tpl', $data);
    }
}