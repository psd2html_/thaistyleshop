<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - items_menu.php
 * Date: 16.04.15
 * Time: 10:00
 * Project: thai-style
 */
class ControllerModuleItemsMenu extends Controller
{
    public function index()
    {
        $this->load->language('module/items_menu');
        $data['heading_title'] = $this->language->get('heading_title');
        $data['select_section'] = $this->language->get('select_section');
        $data['text_contact'] = $this->language->get('text_contact');

        $this->load->model('module/items_menu');

        $data['informations'] = array();

        foreach ($this->model_module_items_menu->getLeftMenu() as $result) {
            $data['informations'][] = array(
                'title' => $result['title'],
                'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
            );
        }
        $data['contact'] = $this->url->link('information/contact');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/items_menu.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/items_menu.tpl', $data);
        } else {
            return $this->load->view('default/template/module/items_menu.tpl', $data);
        }

    }
}