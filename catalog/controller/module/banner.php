<?php
class ControllerModuleBanner extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->addStyle('catalog/view/theme/thai-style/stylesheet/owl.carousel.css');
		$this->document->addScript('catalog/view/theme/thai-style/js/owl.carousel.min.js');

		$data['banners'] = array();
		$data['base'] = $this->config->get('config_url');

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image2'])) {
				$data['banners'][] = array(
					'title' => html_entity_decode($result['title']),
					'link'  => $result['link'],
					'image' => !empty($result['image']) ? 'style="background: url(\'image/' . $result['image'] . '\') top right no-repeat;"' : '',
                    'image2' => 'image/' . $result['image2'],
                    'image3' => !empty($result['image3']) ? 'style="background: url(\'image/' . $result['image3'] . '\') no-repeat;"' : '',
				);
			}

		}

		$data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/banner.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/banner.tpl', $data);
		} else {
			return $this->load->view('default/template/module/banner.tpl', $data);
		}
	}
}