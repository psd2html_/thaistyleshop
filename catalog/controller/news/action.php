<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - action.php
 * Date: 05.05.15
 * Time: 01:18
 * Project: thai-style
 */
class ControllerNewsAction extends Controller {
    public function index() {

        if (!empty($this->request->get['news_id'])) {

            return $this->item();

        } else {

            $this->language->load('information/news');

            $this->load->model('extension/news');
            $this->document->setTitle($this->language->get('heading_title'));

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' 		=> 'Главная',
                'href' 		=> $this->url->link('common/home')
            );
            $data['breadcrumbs'][] = array(
                'text' 		=> 'Акции и спецпредложения',
                'href' 		=> $this->url->link('news/action')
            );

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }

            $filter_data = array(
                'page' 	=> $page,
                'limit' => 10,
                'start' => 10 * ($page - 1),
            );

            $total = $this->model_extension_news->getTotalNews();

            $pagination = new Pagination();
            $pagination->total = $total;
            $pagination->page = $page;
            $pagination->limit = 10;
            $pagination->url = $this->url->link('news/action', 'page={page}');

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($total - 10)) ? $total : ((($page - 1) * 10) + 10), $total, ceil($total / 10));

            $data['heading_title'] = 'Новые акции';
            $data['text_title'] = $this->language->get('text_title');
            $data['text_description'] = $this->language->get('text_description');
            $data['text_date'] = $this->language->get('text_date');
            $data['text_view'] = $this->language->get('text_view');

            $all_news = $this->model_extension_news->getAllNews($filter_data);

            $data['all_news'] = array();

            $this->load->model('tool/image');

            foreach ($all_news as $news) {
                $description = explode ("\n",html_entity_decode($news['short_description']));
                $data['all_news'][] = array (
                    'title' 		=> $news['title'],
                    'image_title' 		=> html_entity_decode($news['image_title']),
                    'image'			=> $this->model_tool_image->resize($news['image'], 871, 390),
                    'description' 	=> !empty($description) ? '<p>' . implode('</p><p>',$description) . '</p>' : '',
                    'period' 		=> strip_tags(html_entity_decode($news['period'])),
                    //'view' 			=> $this->url->link('news/action', 'news_id=' . $news['news_id']),
                    'view' 			=> 'action/' . $news['alias'],
                    'date_added' 	=> date($this->language->get('date_format_short'), strtotime($news['date_added']))
                );
            }

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/news/action.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/news/action.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/news_list.tpl', $data));
            }
        }
    }

    public function item() {
        $this->load->model('extension/news');

        $this->language->load('information/news');

        if (!empty($this->request->get['news_id'])) {
            $news_id = $this->request->get['news_id'];
        } else {
            $news_id = 0;
        }

        $news = $this->model_extension_news->getNews($news_id);

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' 			=> 'Главная',
            'href' 			=> $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Акции и спецпредложения',
            'href' => $this->url->link('news/action')
        );

        if ($news) {
            $data['breadcrumbs'][] = array(
                'text' 		=> $news['title'],
                'href' 		=> 'action/' . $news['alias']  //$this->url->link('information/news/news', 'news_id=' . $news_id)
            );

            $this->document->setTitle($news['title']);
            $this->document->setDescription($news['meta_description']);

            $data['menu'] = $this->model_extension_news->getMenu();

            $data['heading_title'] = $news['title'];
            $data['news_id'] = $news['news_id'];
            $data['description'] = html_entity_decode($news['description']);
            $data['link_action'] = $this->url->link('news/action');

            $this->load->model('catalog/product');
            $data['products'] = array();
            $results = $this->model_catalog_product->getProductAction($news['news_id']);

            if (!empty($results)) {
                $this->load->model('tool/image');

                $data['image'] = $this->model_tool_image->resize($news['image'], 871, 390);

                if (!isset($this->session->data['wishlist'])) {
                    $this->session->data['wishlist'] = array();
                }
                $data['wishlist_array'] = $this->session->data['wishlist'];

                $data['item_cart'] = $this->cart->isArray();

                foreach($results as $result) {

                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = number_format($result['price'], 2, '.', ' '); //$this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$result['special']) {
                        $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = (int)$result['rating'];
                    } else {
                        $rating = false;
                    }

                    $data['products'][] = array(
                        'product_id'  => $result['product_id'],
                        'item_cart'   => in_array($result['product_id'],$data['item_cart']) ? 1 : false,
                        'item_wishlist' => in_array($result['product_id'],$data['wishlist_array']) ? 1 : false,
                        'thumb'       => $image,
                        'name'        => $result['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price'       => $price,
                        'hit'         => $result['hit'],
                        'new'         => $result['new'],
                        'discounts'   => $result['discounts'],
                        'width'       => (int)$result['width'],
                        'manufacturer'=> $result['manufacturer'],
                        'special'     => $special,
                        'tax'         => $tax,
                        'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'rating'      => $rating,
                        'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                    );
                }
            }

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/news/item.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/news/item.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/news.tpl', $data));
            }
        } else {
            $data['breadcrumbs'][] = array(
                'text' 		=> $this->language->get('text_error'),
                'href' 		=> $this->url->link('information/news', 'news_id=' . $news_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = 'Ошибка 404';
            $data['text_error'] = '';
            $data['button_continue'] = $this->language->get('button_continue');
            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }

    }
}