<?php
// Heading
$_['heading_title']    = 'Подписка на новости';
$_['text_home'] = 'Главная';
// Text
$_['text_account']     = 'Личный Кабинет';
$_['text_newsletter']  = 'Рассылка';
$_['text_success']     = 'Ваша подписка успешно обновлена!';

// Entry
$_['entry_newsletter'] = 'Подписаться';


