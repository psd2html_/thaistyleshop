<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <div id="review_c"></div>
        <!-- end.breadcrumbs -->

        <div class="page-content user">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="row">
                    <?php echo $column_left; ?>
                    <div class="col-m8 col-b9 col-s12">
                        <nav class="usermenu-top">
                            <ul class="usermenu-top__list">
                                <li><a href="<?php echo $account; ?>">Персональные данные</a></li>
                                <li class="current"><a href="<?php echo $address; ?>">Адреса доставки</a></li>
                                <li><a href="<?php echo $order; ?>">История заказов</a></li>
                                <li><a href="<?php echo $wishlist; ?>">Избранное</a></li>
                            </ul>
                        </nav>
                        <?php if ($success) { ?>
                        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
                        <?php } ?>
                        <?php if (!empty($error_warning)) { ?>
                        <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                        <?php } ?>
                        <div class="text-content">
                            <div class="row">
                                <div class="formkey col-b6 col-m12">
                                    <h4 class="user-form-head">Добавить адрес</h4>
                                    <p><label for="city">Город:</label>
                                        <input type="text" name="city" value="<?php echo $city; ?>" class="inputbox" id="city">
                                        <div class="text-danger"><?php echo $error_city; ?></div>
                                    </p>
                                    <p><label for="postcode">Индекс:</label>
                                        <input type="text" name="postcode" value="<?php echo $postcode; ?>" class="inputbox" id="postcode">
                                        <div class="text-danger"><?php echo $error_postcode; ?></div>
                                    </p>
                                    <p><label for="address_1">Улица:</label>
                                        <input type="text" name="address_1" value="<?php echo $address_1; ?>" class="inputbox" id="address_1">
                                        <div class="text-danger"><?php echo $error_address_1; ?></div>
                                    </p>
                                    <div class="b-input">
                                        <div class="col-b4 col-s4">
                                            <label for="house">Дом:</label>
                                            <input type="text" name="house" value="<?php echo $house; ?>" class="inputbox" id="house">
                                            <div class="text-danger"><?php echo $error_house; ?></div>
                                        </div>
                                        <div class="col-b4 col-s4">
                                            <label for="housing">Корпус:</label>
                                            <input type="text" name="housing" value="<?php echo $housing; ?>" class="inputbox" id="housing">
                                            <div class="text-danger"><?php echo $error_housing; ?></div>
                                        </div>
                                        <div class="col-b4 col-s4">
                                            <label for="apartment">Квартира:</label>
                                            <input type="text" name="apartment" value="<?php echo $apartment; ?>" class="inputbox" id="apartment">
                                            <div class="text-danger"><?php echo $error_apartment; ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-b6 col-m12">
                                    <?php if ($addresses) { ?>
                                    <h4 class="user-form-head">Сохранённые адреса</h4>
                                    <table class="user-adress">
                                        <?php foreach ($addresses as $result) { ?>
                                        <tr>
                                            <td class="adress-check instylch">
                                                <input type="radio" name="defrad" class="checkbox-big" data-id="<?php echo $result['address_id']; ?>" id="a<?php echo $result['address_id']; ?>" value="1" <?php echo(!empty($result['default']) ? ' checked' : ''); ?> /> <label for="a<?php echo $result['address_id']; ?>"><?php echo $result['address']; ?></label>
                                            </td>
                                            <td class="del">
                                                <a href="<?php echo $result['update']; ?>" class="edit-link">Изменить </a><br/>
                                                <a href="<?php echo $result['delete']; ?>" class="del-link">Удалить</a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                    <?php } ?>
                                </div>
								 
                            </div>
                            <p class="green"><?php echo $personal_discount; ?></p>
                        </div>
                    </div>
                </div>
                <div class="user-btn">
                    <a href="<?php $back_catalog; ?>" class="back-in-cat back-in-cat__top btn">Вернуться в каталог</a>
                    <input type="submit" class="btn btn-purple btn-disabled" value="Сохранить" disabled>
                </div>
            </form>
            <a href="<?php $back_catalog; ?>" class="back-in-cat back-in-cat__bottom btn">Вернуться в каталог</a>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    // Sort the custom fields
    $(document).ready(function() {
        disBut();

        $('.formkey input').keyup(function() {
            disBut();
        });

        $(".user-adress input:radio").click(function() {
            var dataId = $(this).data('id');
            $.ajax({
                url: '<?php echo $url_default; ?>',
                type: 'post',
                dataType: 'json',
                data: {iddefault: dataId},
                cache: false,
                success: function(json) {

                    if (json['error']) {
                        $('#review_c').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        alert(json['success']);

                        //$('#review_c').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });

    function disBut() {
        if ($('.formkey input[name="city"]').val().length > 2 && $('.formkey input[name="address_1"]').val().length > 2 && $('.formkey input[name="house"]').val().length > 0) {
            $('.user-btn input[type="submit"]').removeClass('btn-disabled').prop('disabled', false);
        } else {
            $('.user-btn input[type="submit"]').addClass('btn-disabled').prop('disabled', true);
        }
    }
    //--></script>
<?php echo $footer; ?>