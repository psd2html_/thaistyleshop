<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <h1 class="page-title"><?php echo $heading_title; ?></h1>
        <div class="page-content">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-s12">
                    <div class="text-content">
                        <div class="row">
                            <div class="well">
                                <h4 class="user-form-head"><?php echo $entry_newsletter; ?></h4>
                                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <?php if ($newsletter) { ?>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="1" checked="checked" />
                                        Да </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="0" />
                                        Нет</label>
                                    <?php } else { ?>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="1" />
                                        Да </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="0" checked="checked" />
                                        Нет</label>
                                    <?php } ?>
                                    <div class="buttons clearfix user-btn">
                                        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default">Вернуться назад</a></div>
                                        <div class="pull-right">
                                            <input type="submit" value="Изменить" class="btn btn-purple" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>