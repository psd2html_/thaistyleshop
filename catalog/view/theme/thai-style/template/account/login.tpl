<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <h1 class="page-title"><?php echo $heading_title; ?></h1>
        <div class="page-content">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-s12">
                    <div class="text-content">
                        <div class="row">
                            <?php if ($success) { ?>
                            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
                            <?php } ?>
                            <?php if ($error_warning) { ?>
                            <div class="error-panel"><?php echo $error_warning; ?></div>
                            <?php } ?>
                            <div class=" col-b6 col-m12">
                                <div class="well">
                                    <h4><?php echo $text_new_customer; ?></h4>
                                    <p><strong><?php echo $text_register; ?></strong></p>
                                    <p><?php echo $text_register_account; ?></p>
                                    <a href="<?php echo $register; ?>" class="btn btn-primary">Регистрация</a>
                                </div>
                            </div>
                            <div class=" col-b6 col-m12">
                                <div class="well">
                                    <h4 class="user-form-head"><?php echo $text_returning_customer; ?></h4>
                                    <p><strong><?php echo $text_i_am_returning_customer; ?></strong></p>
                                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                                            <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control inputbox" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                                            <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control inputbox" /><br/>
                                            <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
                                        <div class="user-btn">
                                            <input type="submit" value="Вход" class="btn btn-purple" />
                                        </div>
                                        <?php if ($redirect) { ?>
                                        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>