<span class="shadow-help"></span>
<table class="order-info__table">
    <?php foreach ($products as $product) { ?>
    <tr>
        <td rowspan="2">
            <a target="_blank" href="<?php echo $product['href']; ?>" class="prod-img"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>"></a>
        </td>
        <td class="prod-name"><?php echo $product['name']; ?></td>
    </tr>
    <tr>
        <td>
            <span class="prod-qty"><?php echo $product['quantity']; ?></span>
            <span class="prod-price"><?php echo $product['price']; ?> <span class="rub">д</span></span>
        </td>
    </tr>
    <?php } ?>
</table>
<div class="order-info__check">
    <?php foreach ($totals as $total) { ?>
        <?php if ($total['code'] == 'sub_total') { ?>
            <div class="order-info__subtotal">Подытог <span class="pull-right"><?php echo $total['text']; ?> </span></div>
        <?php } elseif ($total['code'] == 'shipping') { ?>
            <div class="order-info__shipping">Доставка <span class="pull-right"><?php echo $total['text']; ?></span></div>
        <?php  } elseif ($total['code'] == 'register') { ?>
            <div class="order-info__shipping"><?php echo $total['title']; ?> <span class="pull-right"><?php echo $total['text']; ?></span></div>
        <?php  } elseif ($total['code'] == 'personal') { ?>
            <div class="order-info__shipping"><?php echo $total['title']; ?> <span class="pull-right"><?php echo $total['text']; ?></span></div>
        <?php  } elseif ($total['code'] == 'total') { ?>
            <div class="order-info__total">ИТОГО <span class="pull-right"><?php echo $total['text']; ?> </span></div>
        <?php } else { ?>
            <div class="order-info"><?php echo $total['title']; ?> <span class="pull-right"><?php echo $total['text']; ?></span></div>
        <?php } ?>
    <?php } ?>
</div>