<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="page-content user user-favourite-page">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-s12">
                    <nav class="usermenu-top">
                        <ul class="usermenu-top__list">
                            <li><a href="<?php echo $account; ?>">Персональные данные</a></li>
                            <li><a href="<?php echo $address; ?>">Адреса доставки</a></li>
                            <li><a href="<?php echo $order; ?>">История заказов</a></li>
                            <li class="current"><a href="<?php echo $wishlist; ?>">Избранное</a></li>
                        </ul>
                    </nav>
                    <div class="text-content">
                        <!-- products list -->
                        <div id="user-favourite">
                            <div class="row">
                                <?php if ($products) { ?>
                                <?php foreach ($products as $product) { ?>
                                <div class="col-b4 col-m6 col-xs12 wishlist-<?php echo $product['product_id']; ?>">
                                    <div class="product-item">
                                        <a href="<?php echo $product['href']; ?>">
                                            <?php if ($product['new']) { ?>
                                            <span class="product-label new">NEW</span>
                                            <?php } ?>
                                            <?php if ($product['hit']) { ?>
                                            <span class="product-label hit">ХИТ</span>
                                            <?php } ?>
                                            <?php if ($product['discounts']) { ?>
                                            <span class="product-label sale">-<?php echo $product['discounts']; ?>%</span>
                                            <?php } ?>
                                            <div class="product-name">
                                                <?php echo (!empty($product['manufacturer']) ? $product['manufacturer'] . ' - ' : '') . $product['name']  . ' ' . $product['weight']; ?>
                                            </div>
                                            <div class="product-img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></div>
                                            <div class="product-price">
                                                <?php if ($product['price']) { ?>
                                                <?php echo $product['price']; ?> <span class="rub">д</span>
                                                <?php } ?>
                                            </div>
                                        </a>
                                        <!-- hover link -->
                                        <div class="product-link">
                                            <a href="javascript:void(0);" class="popup-fav in-fav select" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"><span>Удалить</span></a>
                                            <a href="javascript:void(0);" class="in-cart<?php if($product['item_cart']) { echo ' select'; } ?>"  onclick="cart.add('<?php echo $product['product_id']; ?>');" title="Добавить в корзину"><span>В корзину</span></a>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php } else { ?>
                                <p><?php echo $text_empty; ?></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-btn">
                <a href="<?php $back_catalog; ?>" class="back-in-cat back-in-cat__top btn">Вернуться в каталог</a>
            </div>
            <a href="<?php $back_catalog; ?>" class="back-in-cat back-in-cat__bottom btn">Вернуться в каталог</a>
        </div>
    </div>
</div>
<?php echo $footer; ?>