<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="page-content user user-orders-page">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-s12">
                    <nav class="usermenu-top">
                        <ul class="usermenu-top__list">
                            <li><a href="<?php echo $account; ?>">Персональные данные</a></li>
                            <li><a href="<?php echo $address; ?>">Адреса доставки</a></li>
                            <li class="current"><a href="<?php echo $order; ?>">История заказов</a></li>
                            <li><a href="<?php echo $wishlist; ?>">Избранное</a></li>
                        </ul>
                    </nav>
                    <div class="text-content">
                        <?php if ($orders) { ?>
                        <div class="user-orders-table">
                            <div class="user-orders__head row">
                                <div class="user-orders__td num">Заказ</div>
                                <div class="user-orders__td date">Дата</div>
                                <div class="user-orders__td sum">Сумма заказа</div>
                                <div class="user-orders__td status">Статус</div>
                                <div class="user-orders__td arrow"></div>
                            </div>
                            <?php foreach ($orders as $order) { ?>
                            <div class="user-orders row" data-href="<?php echo $order['href']; ?>">
                                <div class="user-orders__td num"><?php echo $order['order_id']; ?></div>
                                <div class="user-orders__td date"><?php echo $order['date_added']; ?></div>
                                <div class="user-orders__td sum"><?php echo (!empty($order['total']) ? $order['total'] . '<span class="rub">д</span>' : ''); ?> </div>
                                <div class="user-orders__td status"><?php echo $order['status']; ?></div>
                                <div class="user-orders__td arrow"><span class="user-orders__arrow"></span></div>
                                <div class="order-info">
                                    <img src="/image/preloader.gif" class="imgcenter">
                                </div>
                            </div>
                            <?php } ?>
                            <div class="text-right"><?php echo $pagination; ?></div>
                        </div>
                        <?php } else { ?>
                        <p>Вы ещё не совершали покупок.</p>
                        <?php } ?>
                        <p class="green"><?php echo $personal_discount; ?></p>
                    </div>
                </div>
            </div>
            <div class="user-btn">
                <a href="<?php $back_catalog; ?>" class="back-in-cat back-in-cat__top btn">Вернуться в каталог</a>
            </div>
            <a href="<?php $back_catalog; ?>" class="back-in-cat back-in-cat__bottom btn">Вернуться в каталог</a>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $(document).ready(function() {
        $( ".user-orders" ).click(function() {
            if (!$(this).hasClass( "order-open" ) && !$(this).find('.order-info__table').length) {
                var objOrserInfo = $(this).find('.order-info');
                $.ajax({
                    url: $(this).data('href'),
                    type: 'post',
                    dataType: 'html',
                    cache: false,
                    success: function(html) {
                        objOrserInfo.html(html);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        });
    });
//--></script>
<?php echo $footer; ?>