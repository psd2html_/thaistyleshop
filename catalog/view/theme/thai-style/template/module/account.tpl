<nav class="leftside usermenu">
    <h3>Личный кабинет</h3>
    <a href="#" class="toogle-leftmenu-user">Выбрать другой раздел</a>
    <ul class="leftside-nav">
        <?php if (!$logged) { ?>
            <li<?php if($login == $urlt) { echo ' class="current"'; } ?>><a href="<?php echo $login; ?>" class="list-group-item"><?php echo $text_login; ?></a></li>
            <li<?php if($register == $urlt) { echo ' class="current"'; } ?>><a href="<?php echo $register; ?>" class="list-group-item"><?php echo $text_register; ?></a></li>
            <li<?php if($forgotten == $urlt) { echo ' class="current"'; } ?>><a href="<?php echo $forgotten; ?>" class="list-group-item"><?php echo $text_forgotten; ?></a></li>
        <?php } ?>
        <?php if ($logged) { ?>
            <li<?php if($edit == $urlt) { echo ' class="current"'; } ?>><a href="<?php echo $edit; ?>" class="list-group-item"><?php echo $text_edit; ?></a></li>
        <?php } ?>
            <li<?php if($address == $urlt) { echo ' class="current"'; } ?>><a href="<?php echo $address; ?>" class="list-group-item"><?php echo $text_address; ?></a></li>
            <li<?php if($order == $urlt) { echo ' class="current"'; } ?>><a href="<?php echo $order; ?>" class="list-group-item"><?php echo $text_order; ?></a></li>
            <li<?php if($wishlist == $urlt) { echo ' class="current"'; } ?>><a href="<?php echo $wishlist; ?>" class="list-group-item"><?php echo $text_wishlist; ?></a></li>
        <?php if ($logged) { ?>
            <!--<li><a href="<?php echo $logout; ?>" class="list-group-item"><?php echo $text_logout; ?></a></li>-->
        <?php } ?>
    </ul>
</nav>
