<!-- catalog -->
<div class="mainpage-cat">
    <div class="row">
        <?php foreach ($categories as $key=>$category) { ?>
        <div class="col-b4 col-m6 col-xs12">
            <a href="<?php echo $category['href']; ?>" class="mainpage-cat__item cat<?php echo ($key + 1); ?>">
                <span class="mainpage-cat__title"><?php echo $category['name']; ?></span>
                <?php if(!empty($category['image'])) { ?>
                <img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" class="mainpage-cat__img">
                <?php } ?>
            </a>
        </div>
        <?php } ?>
    </div>
</div>