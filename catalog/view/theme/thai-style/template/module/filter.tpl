<!-- full filter -->
<aside id="shop-filter">
    <div class="leftside">
        <span class="filter-hide">Свернуть</span>
        <form action="/">
            <h3>Фильтр</h3>
            <div class="b-filter price-select">
                <p class="b-filter__title">Диапазон цен</p>
                <div id="price-slider"></div>
                <div class="price-slider__num">
                    <span class="price-from"><?php echo $value_min_max['min']; ?>р.</span>
                    <span class="price-to"><?php echo $value_min_max['max']; ?>р.</span>
                </div>
                <input type="hidden" name = "fprice_from" value="<?php echo $value_min_max['min']; ?>" id="fprice_from"  />
                <input type="hidden" name = "fprice_to" value="<?php echo $value_min_max['max']; ?>" id="fprice_to"  />
            </div>

            <div class="b-filter sort-select">
                <p class="b-filter__title">Сортировка</p>
                <input type="radio" name="sort" id="f1-1" value="p.new&order=DESC"<?php if ($url_sort == 'p.new' && $url_order == 'DESC') { echo ' checked ';} ?>/> <label for="f1-1">Новые сверху</label><br/>
                <input type="radio" name="sort" id="f1-2" value="p.price&order=DESC"<?php if ($url_sort == 'p.price' && $url_order == 'DESC') { echo ' checked ';} ?>/> <label for="f1-2">Цены по убыванию</label><br/>
                <input type="radio" name="sort" id="f1-3" value="p.price&order=ASC"<?php if ($url_sort == 'p.price' && $url_order == 'ASC') { echo ' checked ';} ?>/> <label for="f1-3">Цены по возрастанию</label><br/>
            </div>

            <?php foreach ($filter_groups as $filter_group) { ?>
                <?php if($filter_group['name'] == 'Категории') {
                    $s_name = 'cat-';
                } elseif ($filter_group['name'] == 'Бренды') {
                    $s_name = 'brand-';
                } else {
                    $s_name = '';
                }
                ?>
            <div class="b-filter <?php echo $s_name; ?>select">
                <p class="b-filter__title"><?php echo $filter_group['name']; ?></p>
                <?php foreach ($filter_group['filter'] as $filter) { ?>
                <input type="checkbox" name="filter[]" id="f<?php echo $filter_group['filter_group_id']; ?>-<?php echo $filter['filter_id']; ?>" value="<?php echo $filter['filter_id']; ?>" <?php if(in_array($filter['filter_id'], $filter_category)) { echo ' checked '; } ?> />
                <label for="f<?php echo $filter_group['filter_group_id']; ?>-<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></label><br/>
                <?php } ?>
            </div>
            <?php } ?>

            <div class="b-filter__btn">
                <input type="button" class="btn" id="button-filter" value="Применить">
                <input type="reset" class="reset-btn" id="button-reset" value="Сбросить фильтр">
            </div>
        </form>
    </div>
    <!-- up -->
    <div class="btn-up"></div>
</aside>
<!-- mob-filter -->
<div class="mob-filter">
    <div class="leftside">
        <h3>Фильтр</h3>
        <div class="b-filter price-select">
            <p class="b-filter__title">Диапазон цен</p>
        </div>
        <div class="b-filter sort-select">
            <p class="b-filter__title">Сортировка</p>
        </div>
        <div class="b-filter cat-select">
            <p class="b-filter__title">Категории</p>
        </div>
        <div class="b-filter brand-select">
            <p class="b-filter__title">Бренды</p>
        </div>
    </div>
    <!-- up -->
    <div class="btn-up"></div>
</div>

<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	filter = [];

	$('input[name^=\'filter\']:checked').each(function(element) {
		filter.push(this.value);
	});

    sort = '';
    val_sort = $('input:radio[name=sort]:checked').val();

    if (val_sort) {
        sort = '&sort=' + val_sort;
    }

    priceFrom = '';
    priceFormValue = $('input:hidden[name=fprice_from]').val();
    if (priceFormValue > <?php echo $min_max['min']; ?>) {
        priceFrom = '&fprice_from=' + priceFormValue;
    }

    priceTo = '';
    priceToValue = $('input:hidden[name=fprice_to]').val();
    if (priceToValue < <?php echo $min_max['max']; ?>) {
        priceTo = '&fprice_to=' + priceToValue;
    }

	location = '<?php echo $action; ?>' + ( filter.length > 0 ? '&filter=' + filter.join(',') : '') + sort + priceFrom + priceTo;
});
    $('#button-reset').on('click', function() {
        location = '<?php echo $action; ?>';
    });

    $(window).load(function() {
        equalheight('.product-item');
        equalheight('.product-name');

        //slider
        var left1 = $('.ui-slider-handle').eq(0).position().left;
        $('.price-from').css('left', left1 - 17);

        var left2 = $('.ui-slider-handle').eq(1).position().left;
        $('.price-to').css('left', left2 - 17);

        var width = Math.max($(window).width(), window.innerWidth);
        if(width > 950) {
            $('#shop-filter').css('display', 'block');
        }else{
            $('#shop-filter').hide().css('opacity', 1);
        }
    });

    $(document).ready(function () {
        //slider in filter
        $( "#price-slider" ).slider({
            range: true,
            min: 0,
            max: <?php echo $min_max['max']; ?>,
            values: [ <?php echo $value_min_max['min']; ?>, <?php echo $value_min_max['max']; ?> ],
            slide: function( event, ui ) {
                jQuery( "#fprice_from" ).val( ui.values[ 0 ]);
                var left1 = $('.ui-slider-handle').eq(0).position().left;
                $('.price-from').text(ui.values[ 0 ] + "р.").css('left', left1 - 17);

                jQuery( "#fprice_to" ).val( ui.values[ 1 ]);
                var left2 = $('.ui-slider-handle').eq(1).position().left;
                $('.price-to').text(ui.values[ 1 ] + "р.").css('left', left2 - 17);
            }
        });

        //open mob-filter
        $('.mob-filter').on('click', function(e){
            $('#shop-filter').fadeIn(200);
        });
        $('.filter-hide').on('click', function(e){
            $('#shop-filter').fadeOut(200);
        });

        $(window).resize(function(e) {
            var width = Math.max($(window).width(), window.innerWidth);

            equalheight('.product-item');
            equalheight('.product-name');

            //slider
            var width = Math.max($(window).width(), window.innerWidth);
            if(width > 950) {
                $('#shop-filter').css('display', 'block');
            }else{
                $('#shop-filter').css('display', 'block').css('opacity', 0);
            }
            var left1 = $('.ui-slider-handle').eq(0).position().left;
            $('.price-from').css('left', left1 - 10);

            var left2 = $('.ui-slider-handle').eq(1).position().left;
            $('.price-to').css('left', left2 - 28);

            if(width < 950) {
                $('#shop-filter').hide().css('opacity', 1);
            }
        });

    //add-to-cart click
    function toggleCart(){
        var miniCart = $('.b-topmenu__cart');
        miniCart.toggleClass('hover');
        miniCart.find('.topmenu-dd').fadeToggle(200);
        miniCart.find('.shadow-help').fadeToggle(200);
        miniCart.next('li').toggleClass('next-li');
    }

    $('.in-cart').on('click', function(e){
        e.preventDefault();
        toggleCart();
        $(this).unbind();

        setTimeout(function(){
            if(!$(".b-topmenu__cart").find('.topmenu-dd').is(":hover")){
                toggleCart();
            }
        }, 1500);
    });


    });
//--></script>
