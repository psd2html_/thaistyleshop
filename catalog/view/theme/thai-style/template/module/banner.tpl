<!-- panels of our slider -->
<div class="col-b9 col-m12 slider-wrap">
    <div id="sliderPanels" class="slider-panels">
        <?php foreach ($banners as $banner) { ?>
        <div class="sliderPanel"<?php echo $base . $banner['image']; ?>>
            <span class="sliderPanel__logo" <?php echo $base . $banner['image3']; ?>></span>
            <div class="sliderPanel__name"><?php echo $banner['title']; ?></div>
            <img class="sliderPanel__img" src="<?php echo $base . $banner['image2']; ?>" alt="<?php echo $banner['title']; ?>">
        </div>
        <?php } ?>
    </div>
    <!-- nav -->
    <div class="fp-pager"></div>
    <div class="slider-nav-arrow">
        <span class="fp-slider-prev"></span>
        <span class="fp-slider-next"></span>
    </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function () {
    //slider frontpage
    $('#sliderPanels').carouFredSel({
        responsive: true,
        items: {
            visible: 1,
            width: $('.slider-wrap').width()
        },
        swipe: {
            onTouch: true
        },
        auto: false,
        prev: '.fp-slider-prev',
        next: '.fp-slider-next',
        pagination  : ".fp-pager",
        height: 390,
        circular: true
    });

    //slider arrow
    $('.slider-wrap').hover(
            function(){
                $(this).find('.fp-slider-prev').animate({left: "25px"}, 400);
                $(this).find('.fp-slider-next').animate({right: "25px"}, 400);
            },
            function(){
                $(this).find('.fp-slider-prev').animate({left: "-100px"}, 400);
                $(this).find('.fp-slider-next').animate({right: "-100px"}, 400);
            });


    $(window).resize(function(e) {
        resizeban();
    });

    resizeban();

    $('.sliderPanel__img').each(function( index ) {
        $(this).load(function() {
            var sliderImg = $(this).height();
            $(this).css('margin-top', -sliderImg/2);
        });
    });

});
    function resizeban() {
        $('.sliderPanel__img').each(function( index ) {
            var sliderImg = $(this).height();
            $(this).css('margin-top', -sliderImg/2);
        });
    }
--></script>
