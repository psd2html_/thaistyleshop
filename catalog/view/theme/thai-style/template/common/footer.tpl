<!-- footer -->
<footer>
    <div class="wrap">
        <div class="row">
            <div class="col-m4 col-s6 footer-mod">
                <h4>Основные разделы</h4>
                <ul class="footer-menu">
                    <li><a href="<?php echo $manufacturer; ?>">Бренды</a></li>
                    <li><a href="<?php echo $producthref; ?>">Продукты</a></li>
                    <?php foreach ($categories as $cat) { ?>
                    <li><a<?php echo ($cat['blank'] == 1 ? ' target="_blank" ' : ''); ?> href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
                    <?php } ?>
                    <li><a href="<?php echo $sertificates; ?>">Сертификаты</a></li>
                    <li><a href="<?php echo $news; ?>">Акции</a></li>
                    <li><a href="<?php echo $contact; ?>">Контакты</a></li>
                </ul>
            </div>
            <div class="col-m4 col-s6 footer-mod footer-contact">
                <h4>Контакты</h4>
                <p>
                    <a href="tel:+74959891440"><?php echo $telephone; ?></a><br>
                    <a href="tel:+79032222330">+7 (903) 222-23-30</a><br>
                    <a href="mailto:><?php echo $email; ?>"><?php echo $email; ?></a><br>
                    <?php echo nl2br($address); ?>
                </p>
            </div>
            <div class="col-m4 col-s12 footer-mod">
                <h4 class="s-hide">Присоединяйтесь к нам</h4>
                <ul class="footer-soc">
                    <?php if (!empty($facebook)) { ?>
                        <li class="fb"><a target="_blank" href="<?php echo $facebook; ?>">Facebook</a></li>
                    <?php } ?>
                    <?php if (!empty($twitter)) { ?>
                    <li class="tw"><a target="_blank" href="<?php echo $twitter; ?>">Twitter</a></li>
                    <?php } ?>
                    <?php if (!empty($vkontakte)) { ?>
                    <li class="vk"><a target="_blank" href="<?php echo $vkontakte; ?>">Вконтакте</a></li>
                    <?php } ?>
                    <?php if (!empty($instagram)) { ?>
                    <li class="insta"><a target="_blank" href="<?php echo $instagram; ?>">Instagram</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <a href="#" id="mob-remove" class="mainsite-link">Перейти на основную версию сайта</a>
    </div>
</footer>
<?php if (!$logged) { ?>
<!-- popups -->
<div class="popup-help" id="reg">
    <div class="popup-header">Регистрация</div>
    <div class="popup-content">
    	<p class="red"><?php echo $text_discount; ?></p>
        <p><label for="r_name">Имя:</label>
            <input type="text" class="inputbox" id="r_name">
        </p>
        <div class="error_firstname"></div>
        <p><label for="r_email">E-mail:</label>
            <input type="email" class="inputbox" id="r_email">
        </p>
        <div class="error_email"></div>
        <p><label for="p_phone">Телефон:</label>
            <input type="text" class="inputbox" id="p_phone">
        </p>
        <div class="error_telephone"></div>
        <p><label for="p_password">Пароль:</label>
            <input type="password" class="inputbox" id="p_password">
        </p>
        <div class="error_password"></div>
        <p><label for="p_c_password">Подтверждение пароля:</label>
            <input type="password" class="inputbox" id="p_c_password">
        </p>
        <div class="error_warning"></div>
        <input type="button" class="btn" value="Отправить">
    </div>
</div>

<div class="popup-help" id="password">
    <div class="popup-header">Вход</div>
    <div class="popup-content">
        <p><label for="pemail">Введите ваш e-mail:</label>
            <input type="text" class="inputbox" id="pemail">
        </p>
        <p>На данный адрес будет выслано письмо для восстановления пароля.</p>
        <div class="perror"></div>
        <input type="button" class="btn" value="Отправить">
    </div>
</div>

<div class="popup-help loginpop" id="login">
    <div class="popup-header">Вход</div>
    <div class="popup-content">
        <p><label for="email">Логин / e-mail:</label>
            <input type="text" class="inputbox pop_email" id="email">
        </p>
        <p><label for="passwird">Пароль:</label>
            <input type="password" class="inputbox pop_password" id="passwird">
        </p>
        <div class="perror"></div>
        <input type="button" class="btn" value="Войти">
        <a class="popup" href="#password">Забыли пароль?</a>
        <p><a class="popup" href="#reg">Регистрация</a></p>
    </div>
</div>
<script type="text/javascript"><!--
    $('#reg').on('click','input[type="button"]',function() {
        var loginName = $('#reg').find('#r_name').val();
        var loginEmail = $('#reg').find('#r_email').val();
        var loginPhone = $('#reg').find('#p_phone').val();
        var loginPassword = $('#reg').find('#p_password').val();
        var loginCPassword = $('#reg').find('#p_c_password').val();
        $.ajax({
            url: '<?php echo $register; ?>',
            type: 'post',
            dataType: 'json',
            data: {firstname: loginName, email: loginEmail, telephone: loginPhone, password: loginPassword, confirm: loginCPassword, ajax: 1},
            cache: false,
            success: function(json) {

                if (json['error']) {

                    $.each(['firstname','email','telephone','password','warning'], function( index, value ) {
                        $('#reg').find('.error_' + index).html('');
                    });

                    $.each(json['error'], function( index, value ) {
                        $('#reg').find('.error_' + index).html('<div class="error-panel">' + value + '</div>');
                    });
                }

                if (json['success']) {
                    $('#reg').find('.popup-content').html('<p class="text-center">Добро пожаловать в ThaiStyleShop и благодарим Вас за регистрацию!</p>');
                    setTimeout(function() {
                                //location.reload();
                                window.location = "/edit-account"
                            }, 2000
                    );
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('#password').on('click','input[type="button"]',function() {
        var loginEmail = $('#password').find('#pemail').val();
        $.ajax({
            url: '<?php echo $action_forgot; ?>',
            type: 'post',
            dataType: 'json',
            data: {email: loginEmail, ajax: 1},
            cache: false,
            success: function(json) {

                if (json['error']) {
                    $('#password').find('.perror').html('<div class="error-panel">' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#password').find('.popup-content').html('<p class="text-center">На Ваш e-mail адрес выслано письмо для восстановления пароля.</p>');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    loginpop();

    function loginpop() {
        $('.loginpop').on('click','input[type="button"]',function() {
            var itlog = $(this).parent();
            var loginName = itlog.find('.pop_email').val();
            var loginPas = itlog.find('.pop_password').val();
            $.ajax({
                url: '<?php echo $action_login; ?>',
                type: 'post',
                dataType: 'json',
                data: {email: loginName, password: loginPas, ajax: 1},
                cache: false,
                success: function(json) {

                    if (json['error']) {
                        itlog.find('.perror').html('<div class="error-panel">' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        itlog.find('.popup-content').html('<p class="text-center">Вы удачно вошли в систему сайта Thai Style!<br/>Удачных покупок!</p>');
                        setTimeout(function() {
                                    location.reload();
                                }, 2000
                        );

                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    }
    //--></script>
<?php } ?>
<div class="popup-help favourite" id="add_wishlist">
    <div class="popup-header">Добавление в избранное</div>
    <div class="popup-content clearfix">
        <div class="fav-img"></div>
        <h4 class="wsl"></h4>
        <div class="fav-login loginpop">
            <p>Товар добавлен в Избранное.<br/>Чтобы посмотреть добавленные в Избранное продукты, Вам необходимо войти или зарегистрироваться.</p>
            <p><label for="l_name">Логин / e-mail:</label>
                <input type="text" class="inputbox pop_email" id="l_name">
            </p>
            <p><label for="l_password">Пароль:</label>
                <input type="password" class="inputbox pop_password" id="l_password">
            </p>
            <div class="perror"></div>
            <input type="button" class="btn" value="Войти"> <a class="popup" href="#password"><span style="white-space: nowrap;">Забыли пароль?</span></a>
            <p><a class="popup" href="#reg">Регистрация</a></p>
        </div>
    </div>
</div>
<!-- script here -->
<script src="catalog/view/theme/thai-style/js/jquery.placeholder.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.easing.1.3.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.jscrollpane.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery-ui.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.mousewheel.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.touchSwipe.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.fancybox.pack.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.zoom.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="catalog/view/theme/thai-style/js/owl.carousel.min.js"></script>
<script src="catalog/view/theme/thai-style/js/main.js"></script>

</body>
</html>