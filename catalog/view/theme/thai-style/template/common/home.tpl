<?php echo $header; ?>
<div id="page">
  <div class="wrap">
      <?php if (!empty($slider_left) && !empty($slider_right)) { ?>
      <div class="b-slider">
          <div id="cat-sliderTabs" class="row">
              <div class="col-b3">
                  <?php echo $slider_left; ?>
              </div>
              <?php echo $slider_right; ?>
          </div>
      </div>
      <?php } ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>