<span class="cart-qty"><?php echo $count_total; ?></span>
<i class="icon-cart"></i>
<?php if($price_total > 0) { ?>
<span class="cart-sum"><?php echo $price_total; ?> <i class="rub">д</i></span>
<?php } ?>
<span class="shadow-help"></span>
<div class="topmenu-dd">
    <h5>КОРЗИНА</h5>
    <div class="minicart__wrap">
        <?php if ($products || $vouchers) { ?>
        <form action="/">
            <div class="minicart__scroll">
                <table class="minicart__table">
                    <?php foreach ($products as $product) { ?>
                    <tr>
                        <td class="img" rowspan="2">
                            <?php if ($product['thumb']) { ?>
                            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                            <?php } ?>
                        </td>
                        <td class="name">
                            <div class="minicart__name">
                                <a href="<?php echo $product['href']; ?>"><?php echo $product['manufacture']; ?>
                                    <span class="minicart__desc"><?php echo $product['name'] . (!empty($product['weight']) ? (' ' . ((int)$product['weight']/$product['quantity']) . ' мл') : ''); ?></span></a>
                            </div>
                        </td>
                        <td class="del">
                            <div class="minicart-del" onclick="cart.remove('<?php echo $product['key']; ?>');">x</div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="minicart__price"><?php echo $product['quantity']; ?>х<?php echo $product['price']; ?> <i class="rub">д</i></p>
                        </td>
                        <td class="minicart__sum"><?php echo $product['total']; ?> <i class="rub">д</i></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            <div class="minicart__check">
                ИТОГО: <span><?php echo $price_total; ?> <i class="rub">д</i></span>
            </div>
            <p style="text-align: center;margin: 0;"><a href="<?php echo $cart; ?>" class="minicart__btn"><span>Оформить заказ</span></a></p>
        </form>
        <?php } else { ?>
        <div class="minicart__check"><?php echo $text_empty; ?></div>
        <?php } ?>
    </div>
</div>