<!DOCTYPE html>
<!--[if lt IE 9]> <html class="no-js ie8"> <![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="catalog/view/theme/thai-style/favicon.ico">
    <?php if ($icon) { ?>
    <!-- favicon.ico -->
    <link href="<?php echo $icon; ?>" rel="icon" />
    <?php } ?>
    <!-- soc meta -->
    <meta property="og:site_name" content="ThaiStyleShop">
    <meta property="og:type" content="website">
    <meta property="og:keywords" content="<?php echo !empty($keywords) ? $keywords : ''; ?>">
    <link rel="image_src" href="">
    <meta property="og:image" content="">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <!-- СSS here -->
    <link rel="stylesheet" href="catalog/view/theme/thai-style/stylesheet/jquery-ui.min.css">
    <link rel="stylesheet" href="catalog/view/theme/thai-style/stylesheet/jquery.fancybox.css">
    <link rel="stylesheet" href="catalog/view/theme/thai-style/stylesheet/owl.carousel.css">
    <link rel="stylesheet" href="catalog/view/theme/thai-style/stylesheet/main.css">
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php echo $google_analytics; ?>
</head>
<body>
<!-- header -->
<header>
    <div class="wrap">
        <div class="b-logo clearfix">
            <a href="<?php echo $home; ?>" class="logo-link" title="<?php echo $name; ?>"></a>
            <div class="logo-text"><?php echo nl2br($meta_description); ?></div>
        </div>

        <div class="mob-menu"></div>
        <div class="b-topmenu-mob">
            <ul class="topmenu-mob__list">
                <li class="topmenu-mob__search"><a href="<?php echo $url_search; ?>"><i class="icon-search"></i>Поиск</a></li>
                <li class="topmenu-mob__login"><a class="popup" href="#login"><i class="icon-user"></i>Вход/регистрация</a></li>
                <li class="topmenu-mob__cart"><a href="<?php echo $shopping_cart; ?>"><span class="cart-qty">6</span><i class="icon-cart"></i>Корзина</a></li>
                <li class="topmenu-mob__share"><i class="icon-share"></i>Поделиться
                    <a href="http://vkontakte.ru/share.php?url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"  target="_blank" class="vk"></a>
                    <a href="http://www.facebook.com/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&t=<?php echo $title; ?>" target="_blank" class="fb"></a>
                </li>
                <?php if(!empty($telephone)) { ?>
                <li class="topmenu-mob__tel"><a href="tel:<?php echo $telephone; ?>"><i class="icon-tel"></i>Звоните нам <?php echo $telephone; ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="b-topmenu">
            <ul class="clearfix">
                <?php if(!empty($telephone)) { ?>
                <!-- tel -->
                <li class="tel">
                    <i class="icon-tel"></i>
                    <span><?php echo $telephone; ?></span><br>
                    <a href="tel:+79032222330">+7 (903) 222-23-30</a><span class="viber"><i>What's up</i> | <i>Viber</i></span>
                    <span class="shadow-help"></span>
                    <div class="topmenu-dd">
                        <h5>ЗВОНИТЕ</h5>
                        <a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a><br>
                        <a href="tel:+79032222330">+7 (903) 222-23-30</a><span class="viber"><i>What's up</i> | <i>Viber</i></span>
                    </div>
                </li>
                <!-- end.tel -->
                <?php } ?>
                <!-- topmenu user -->
                <li class="dd-toggle b-topmenu__user">
                    <i class="icon-user"></i>
                    <span class="shadow-help"></span>
                    <div class="topmenu-dd">
                        <h5>ВАШ АККАУНТ</h5>
                        <ul class="dropdown-menu" role="menu">
                            <?php if ($logged) { ?>
                                <li><a href="<?php echo $account; ?>">Мои данные</a></li>
                                <li><a href="<?php echo $order; ?>">Мои заказы</a></li>
                                <li><a id="wishlist-total" href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                                <li class="logout"><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                            <?php } else { ?>
                                <li><a class="popup" href="#login">Войти</a></li>
                                <li><a class="popup" href="#reg">Регистрация</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>

                <!-- topmenu cart -->
                <li id="cart" class="dd-toggle b-topmenu__cart">
                    <?php echo $cart; ?>
                </li>
                <!-- topmenu share -->
                <li class="dd-toggle b-topmenu__share">
                    <i class="icon-share"></i>
                    <span class="shadow-help"></span>
                    <div class="topmenu-dd">
                        <ul>
                            <li><a href="http://www.facebook.com/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&t=<?php echo $title; ?>" target="_blank" class="fb"></a></li>
                            <li><a href="http://vkontakte.ru/share.php?url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"  target="_blank" class="vk"></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</header>

<!-- mainmenu -->
<div class="b-mainmenu">
    <div class="wrap">
        <ul class="mainmenu-list">
            <li><a href="<?php echo $manufacturer; ?>">Бренды</a></li>
            <li>
                <a href="<?php echo $producthref; ?>">Продукты</a>
                <?php if (!empty($categoriesmenu)) { ?>
                <ul>
                    <?php foreach ($categoriesmenu as $menu) { ?>
                    <li><a href="<?php echo $menu['href']; ?>"><?php echo $menu['name']; ?></a></li>
                    <?php } ?>
                    <li><a href="<?php echo $producthref; ?>">Все продукты</a></li>
                </ul>
                <?php } ?>
            </li>
            <?php foreach ($categories as $cat) { ?>
            <li><a<?php echo ($cat['blank'] == 1 ? ' target="_blank" ' : ''); ?> href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
            <?php } ?>
            <li><a href="<?php echo $news; ?>">Акции</a></li>
            <li class="mainmenu-contact"><a href="<?php echo $contact; ?>">Контакты</a></li>
            <?php if ($logged) { ?>
            <li class="r-toggle"><a href="<?php echo $account; ?>">Мои данные</a></li>
            <li class="r-toggle"><a href="<?php echo $order; ?>">Мои заказы</a></li>
            <li class="r-toggle"><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            <li class="r-toggle"><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li class="r-toggle"><a class="popup" href="#login">Вход</a></li>
            <li class="r-toggle"><a class="popup" href="#reg">Регистрация</a></li>
            <?php } ?>
        </ul>

        <div class="b-search">
            <form>
            <?php echo $search; ?>
            </form>
            <a href="/index.php?route=product/search" class="btn-search"></a>
        </div>

        <!-- mob mainmenu -->
        <ul class="mob-mainmenu clearfix">
            <li><a href="#" id="catmenu-toggle"><i class="icon-prod"></i></a></li>
            <li><a href="<?php echo $shopping_cart; ?>"><i class="icon-cart white"></i></a></li>
            <li class="search-link"><a href="<?php echo $url_search; ?>"><i class="icon-search white"></i></a></li>
            <li><a class="mob-menu-main" href="#"><i class="icon-list"></i></a></li>
        </ul>
        <ul class="cat-menu">
            <?php foreach ($categoriesmenu as $menu) { ?>
            <li><a href="<?php echo $menu['href']; ?>"><?php echo $menu['name']; ?></a></li>
            <?php } ?>
            <li><a href="<?php echo $producthref; ?>">Все продукты</a></li>
        </ul>
    </div>
</div>