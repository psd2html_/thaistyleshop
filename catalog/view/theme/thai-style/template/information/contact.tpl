<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <div class="page-content contact info">
            <h1 class="page-title"><?php echo $heading_title; ?></h1>
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-xs12">
                    <div class="text-content">
                        <div class="row">
                            <div class="col-m3 col-s12">
                                <h3>Обратная связь</h3>
                                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="contact-form">
                                    <p>
                                        <label for="input-name">Имя:</label>
                                        <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control inputbox" />
                                        <?php if ($error_name) { ?>
                                    <div class="error-panel"><?php echo $error_name; ?></div>
                                    <?php } ?>
                                    </p>
                                    <p>
                                        <label for="input-email">E-mail:</label>
                                        <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control inputbox" />
                                        <?php if ($error_email) { ?>
                                    <div class="error-panel"><?php echo $error_email; ?></div>
                                    <?php } ?>
                                    </p>
                                    <p>
                                        <label for="input-enquiry">Сообщение:</label>
                                        <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control inputbox"><?php echo $enquiry; ?></textarea>
                                        <?php if ($error_enquiry) { ?>
                                    <div class="error-panel"><?php echo $error_enquiry; ?></div>
                                    <?php } ?>
                                    </p>
                                    <?php if ($site_key) { ?>
                                    <p>
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                                        <?php if ($error_captcha) { ?>
                                        <div class="error-panel"><?php echo $error_captcha; ?></div>
                                        <?php } ?>
                                    </div>
                                    </p>
                                    <?php } ?>
                                    <input class="btn" type="submit" value="Отправить" />
                                </form>
                            </div>
                            <div class="col-m6 col-s12">
                                <div class="b-border">
                                    <?php echo $content_bottom; ?>
                                </div>
                            </div>
                        </div>
                        <?php if ($geocode) { ?>
                        <!-- map -->
                        <h3>Где мы находимся</h3>
                        <div class="contact-map">
                            <div id="map-canvas"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($geocode) { ?>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript"><!--
    // map
    var mapCenter = new google.maps.LatLng(<?php echo $geocode; ?>);

    function initializeMap() {
        var mapOptions = {
            center: mapCenter,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map-canvas"),
                mapOptions);

        var marker = new google.maps.Marker({
            position: mapCenter
        });
        marker.setMap(map);
    }

    google.maps.event.addDomListener(window, 'load', initializeMap);

//--></script>
<?php } ?>
<?php echo $footer; ?>
