<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->

        <div class="page-content page404">
            <div class="page404__text">
                <p>Ошибка 404</p>
                <p><?php echo $heading_title; ?></p>
                <p><?php echo $text_error; ?></p>
            </div>
            <div class="page404__btn">
                <a href="<?php echo $producthref; ?>" class="btn">Перейти в каталог</a>
            </div>
            <div class="page404__img"><img src="/catalog/view/theme/thai-style/images/img404.png" alt="not_found"></div>
        </div>
    </div>
</div>
<?php echo $footer; ?>