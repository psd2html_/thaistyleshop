<input type="button" class="btn ml0 mt15 btn-checkout" id="button-confirm" value="Отправить заказ">
<script type="text/javascript"><!--
$('#button-confirm').on('click', function() {
	$.ajax({
		type: 'post',
		url: 'index.php?route=payment/cod/confirm',
		cache: false,
		data: $('#order-reg form').serialize(),
		beforeSend: function() {
			//$('#button-confirm').button('loading');
		},
		complete: function() {
			//$('#button-confirm').button('reset');
		},
		success: function(json) {
			if (json.error) {
				$('p.error').remove();
				$.each(json.error, function(i, item) {
					$('input[name="' + i + '"]').after('<p class="error">' + item + '</p>');
				});
			} else {
				location = '<?php echo $continue; ?>';
			}
			//
		}
	});
});
//--></script>
