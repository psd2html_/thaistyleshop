<form class="form-horizontal">
  <p class="checkout-info">ВВЕДИТЕ АДРЕС ДОСТАВКИ</p>
  <?php if ($addresses) { ?>
  <div id="shipping-existing">
    <table class="checkout-table adress">
      <tbody>
      <?php foreach ($addresses as $address) { ?>
      <tr>
        <td class="checkout-table__radio">
          <input class="checkbox-big" type="radio" name="address_id" value="<?php echo $address['address_id']; ?>" <?php if ($address['address_id'] == $address_id) { echo 'checked="checked"';} ?> />
        </td>
        <td class="checkout-table__label">
          <?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo 'ул. ' . $address['address_1'] . ', д. ' . $address['house'] . (!empty($address['housing']) ? ', корп. ' . $address['housing'] : '') . (!empty($address['apartment']) ? ', кв. ' . $address['apartment'] : ''); ?>, <?php echo $address['city']; ?>, <?php echo $address['postcode']; ?>
        </td>
      </tr>
      <?php } ?>
      </tbody>
    </table>
  </div>
  <table class="checkout-table adress">
    <tbody>
    <tr class="checkadr" style="display: none">
      <td class="checkout-table__radio">
        <input class="checkbox-big" type="radio" name="shipping_address" value="existing" checked="checked" />
      </td>
      <td class="checkout-table__label">
        <?php echo $text_address_existing; ?>
      </td>
    </tr>
    <tr class="nocheckadr">
      <td class="checkout-table__radio">
        <input class="checkbox-big" type="radio" name="shipping_address" value="new" />
      </td>
      <td class="checkout-table__label">
        <?php echo $text_address_new; ?>
      </td>
    </tr>
    </tbody>
  </table>
  <?php } ?>
  <br />
  <div id="shipping-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
    <div class="cart-tab-block common">
      <p><label for="input-shipping-postcode">Индекс:</label>
        <input type="text" name="postcode" value="<?php echo $postcode; ?>" id="input-shipping-postcode" class="inputbox" />
      </p>
		<p><label for="input-shipping-city">Город:</label>
			<input type="text" name="city" value="<?php echo $city; ?>" id="input-shipping-city" class="inputbox" />
		</p>
      <p><label for="input-shipping-address-1">Улица:</label>
        <input type="text" name="address_1" value="<?php echo $address_1; ?>" id="input-shipping-address-1" class="inputbox" />
      </p>
      <div class="b-input">
        <div class="col-b4 col-m4">
          <label for="input-shipping-haus">Дом:</label>
          <input type="text" name="haus" value="<?php echo $haus; ?>" id="input-shipping-haus" class="inputbox" />
        </div>
        <div class="col-b4 col-m4">
          <label for="input-shipping-korp">Корпус:</label>
          <input type="text" name="korp" value="<?php echo $korp; ?>" id="input-shipping-korp" class="inputbox" />
        </div>
        <div class="col-b4 col-m4">
          <label for="input-shipping-apart">Квартира:</label>
          <input type="text" name="apart" value="<?php echo $apart; ?>" id="input-shipping-apart" class="inputbox" />
        </div>
      </div>
    </div>
  </div>
  <div class="buttons clearfix" style="display: none">
    <div class="pull-right">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-address" class="btn btn-primary" />
    </div>
  </div>
</form>
<script type="text/javascript"><!--
$('input[name=\'shipping_address\']').on('change', function() {
	if (this.value == 'new') {
		$('#shipping-existing').hide();
		$('#shipping-new').show();

      $('.checkadr').show();
      $('.nocheckadr').hide();

    } else {
		$('#shipping-existing').show();
		$('#shipping-new').hide();

      $('.checkadr').hide();
      $('.nocheckadr').show();
	}
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-shipping-address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-shipping-address .form-group').length) {
		$('#collapse-shipping-address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#collapse-shipping-address .form-group').length) {
		$('#collapse-shipping-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#collapse-shipping-address .form-group').length) {
		$('#collapse-shipping-address .form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-shipping-address button[id^=\'button-shipping-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					//$(node).button('loading');
				},
				complete: function() {
					//$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-shipping-address select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#collapse-shipping-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#collapse-shipping-address input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('#collapse-shipping-address input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('#collapse-shipping-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#collapse-shipping-address select[name=\'country_id\']').trigger('change');
//--></script>