<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<?php $dis = (!$logged && $account != 'guest') ? ' disabled ' : ''  ?>
<p class="checkout-info">Выберите способ оплаты</p>
<table class="checkout-table">
<?php foreach ($payment_methods as $payment_method) { ?>
  <tr>
    <td class="checkout-table__radio">
      <?php if ($payment_method['code'] == $code || !$code) { ?>
      <?php $code = $payment_method['code']; ?>
      <input class="checkbox-big" type="radio" name="payment_method" id="<?php $code; ?>" value="<?php echo $payment_method['code']; ?>"  checked="checked" /> <label for="<?php $code; ?>"><?php echo $payment_method['title']; ?></label>
      <?php } else { ?>
      <input class="checkbox-big" type="radio" name="payment_method" id="<?php $code; ?>" value="<?php echo $payment_method['code']; ?>"  /> <label for="<?php $code; ?>"><?php echo $payment_method['title']; ?></label>
      <?php } ?>
    </td>
    <td class="checkout-table__label">
      <?php echo (!empty($payment_method['description']) ? $payment_method['description'] : ''); ?>
    </td>
    <?php if ($payment_method['terms']) { ?>
    (<?php echo $payment_method['terms']; ?>)
    <?php } ?>
  </tr>
<?php } ?>
</table>
<?php } ?>
<?php if ($text_agree) { ?>
<div class="buttons">
  <div class="pull-right"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    &nbsp;
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="btn btn-primary" />
  </div>
</div>
<?php } else { ?>
<div class="buttons" style="display: none">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="btn btn-primary" />
  </div>
</div>
<?php } ?>
