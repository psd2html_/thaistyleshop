<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>

        <!-- end.breadcrumbs -->
        <div class="page-content cart">
            <h1 class="page-title">Корзина</h1>
            <!-- cart list -->
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="cart-list">
                    <table class="cart-table">
                        <tr>
                            <th>Наименование</th>
                            <th>Цена</th>
                            <th>Количество</th>
                            <th>Сумма</th>
                            <th></th>
                        </tr>
                        <?php foreach ($products as $product) { ?>
                        <tr data-remoid="<?php echo $product['key']; ?>">
                            <td class="name">
                                <?php if ($product['thumb']) { ?>
                                <a class="img" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                                <?php } ?>
                                <a class="name-link" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a><br>
                                <?php if(!$product['stock']) { ?><span class="red">Нет в наличии!</span><?php } ?>
                            </td>
                            <td class="price">
                            	<?php if ($product["original_price"]) { ?>
                            		<div class="old-price"><?php echo $product['original_price']; ?> <span class="rub">д</span></div>
                            		<div class="new-price"><?php echo $product['price']; ?> <span class="rub">д</span></div>
                            	<?php } else { ?>
                            		<?php echo $product['price']; ?> <span class="rub">д</span>
                            	<?php } ?>
                            </td>
                            <td class="qty">
                                <div class="b-cart-qty">
                                    <span class="qty-minus count">−</span>
                                    <input type="text" id="quantity" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>"  class="inputbox" />
                                    <span class="qty-plus count">+</span>
                                    <button type="submit" data-toggle="tooltip" title="Обновить" class="btn btn-no"><i class="fa fa-refresh"></i></button>
                                </div>
                            </td>
                            <td class="summ"><?php echo $product['total']; ?> <span class="rub">д</span></td>
                            <td class="del">
                                <a href="javascript:void(0);" onclick="cart.remove('<?php echo $product['key']; ?>');" class="del-link">Удалить</a>
                            </td>
                        </tr>
                        <?php } ?>

                    </table>
                </div>

                <!-- mob cart -->
                <div class="mob-cart">
                    <?php  foreach ($products as $product) { ?>
                    <div class="mob-cart__item">
                        <a href="javascript:void(0);" onclick="cart.remove('<?php echo $product['key']; ?>');" class="del-link">Удалить</a>
                        <div class="mob-cart__img">
                            <?php if ($product['thumb']) { ?>
                            <a class="img" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                            <?php } ?>
                        </div>
                        <div class="mob-cart__info">
                            <a class="name-link" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>

                            <div class="b-cart-qty">
                                <span class="qty-minus count">−</span>
                                <input type="text" id="quantity" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>"  class="inputbox" />
                                <span class="qty-plus count">+</span>
								<button type="submit" data-toggle="tooltip" title="Обновить" class="btn btn-no"><i class="fa fa-refresh"></i></button>
                            </div>

                            <div class="mob-cart__sum">
                                x <?php echo $product['price']; ?><span class="rub">д</span> = <?php echo $product['total']; ?><span class="rub">д</span>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <!-- promocode -->
                <div class="cart-checkout clearfix">
                    <?php if ($coupon) { ?>
                    <?php echo $coupon; ?>
                    <?php } ?>
                    <div class="b-total">
                        <p class="b-total__sum"><b>ОБЩАЯ СУММА</b> <?php echo $totals['sub_total']['text']; ?><span class="rub">д</span></p>
                        <?php if (!empty($totals['coupon'])) { ?>
                        <p class="b-total__sale"><b>ИТОГО</b> <?php echo $totals['total']['text']; ?><span class="rub">д</span> <span class="sale-num">(скидка <?php echo $totals['coupon']['text']; ?> <span class="rub">д</span>)</span></p>
                        <?php } ?>
                    </div>
                </div>

            </form>
                <!-- cart tabs -->
                <div class="cart-tabs">
                    <ul class="tabs-nav">
                        <?php if(!$logged && $account != 'guest') { ?>
                        <li><a class="active tablink" href="#order-reg">ОФОРМЛЕНИЕ <span class="r-hidden">ЗАКАЗА</span> С РЕГИСТРАЦИЕЙ</a></li>
                        <li><a class="tablink" href="#order-fast">БЫСТРЫЙ ЗАКАЗ</a></li>
                        <li><a class="tablink" href="#order-login"><span class="r-hidden">ЗАКАЗ ДЛЯ ЗАРЕГИСТРИРОВАННЫХ ПОЛЬЗОВАТЕЛЕЙ</span><span class="r-show">ЗАРЕГИСТРИРОВАННЫМ ПОЛЬЗОВАТЕЛЯМ</span></a></li>
                        <?php } else { ?>
                        <li><a class="active tablink" href="#order-reg">ОФОРМЛЕНИЕ <span class="r-hidden">ЗАКАЗА</span></a></li>
                        <?php } ?>
                    </ul>

                    <div class="cart-tab-content">
                        <!-- tab1 -->
                        <div role="tabpanel" class="tab-pane" style="display: block;" id="order-reg">
                    <form action="<?php echo $action3; ?>" method="post" enctype="multipart/form-data">
                                <?php if(!$logged && $account != 'guest') { ?>
                                <input type="hidden" name="regist" value="1">
                                <div class="cart-tab-block common">
                                    <p class="checkout-info red"><?php echo $text_discount; ?></p>
                                    <p><label for="input-orderreg-r_name">Имя:</label>
                                        <input type="text" name="r_name" value="<?php echo $r_name; ?>" id="input-orderreg-r_name" class="inputbox" />
                                    </p>
                                    <p><label for="input-orderreg-r_email">E-mail:</label>
                                        <input type="text" name="r_email" value="<?php echo $r_email; ?>" id="input-orderreg-r_email" class="inputbox" />
                                    </p>
                                    <p><label for="input-orderreg-r_phone">Телефон:</label>
                                        <input type="text" name="r_phone" value="<?php echo $r_phone; ?>" id="input-orderreg-r_phone" class="inputbox" />
                                    </p>
                                    <p><label for="input-orderreg-r_sity">Город:</label>
                                        <input type="text" name="r_sity" value="<?php echo $r_sity; ?>" id="input-orderreg-r_sity" class="inputbox" />
                                    </p>
                                    <p><label for="input-orderreg-r_password">Пароль:</label>
                                        <input type="password" name="r_password" value="<?php echo $r_password; ?>" id="input-orderreg-r_password" class="inputbox" />
                                    </p>
                                    <p><label for="input-orderreg-r_passwordc">Подтверждение пароля:</label>
                                        <input type="password" name="r_passwordc" value="<?php echo $r_passwordc; ?>" id="input-orderreg-r_passwordc" class="inputbox" />
                                    </p>
                                </div>
                                <?php } else { ?>
                                <input type="hidden" name="regist" value="2">
                                <?php } ?>

                                <div class="cart-tab-block shipping">
                                    <div class="panel-collapse collapse" id="collapse-shipping-method">
                                        <div class="panel-body">
                                        </div>
                                    </div>
                                </div>

                                <div class="cart-tab-block adress" id="collapse-shipping-address" style="display: none">
                                </div>
                        <p><label for="input-shipping-comment">Дополнительная информация:</label>
                            <textarea name="comment" id="input-shipping-comment" class="inputbox"><?php echo $comment; ?></textarea>
                        </p>

                                <div class="cart-tab-block pay disabled" id="collapse-payment_method">
                                </div>
                                <div class="panel-collapse collapse" id="collapse-checkout-confirm">
                                </div
                            </form>
                        </div>
                        <?php if(!$logged && $account != 'guest') { ?>
                        <!-- tab2 -->
                        <div role="tabpanel" class="tab-pane" id="order-fast">
                            <form id="orderfast" action="<?php echo $action2; ?>" method="post">
                                <input type="hidden" name="quick" value="1">
                                <div class="cart-tab-block">
                                    <p class="checkout-info">Быстрый заказ действует только для заказов по Москве и Московской области.</p>
                                    <p><label for="q_name">Имя:</label>
                                        <input type="text" name="q_name" value="<?php $q_name; ?>" class="inputbox" id="q_name">
                                    </p>
                                    <p><label for="q_phone">Телефон:</label>
                                        <input type="text" name="q_phone" value="<?php $q_phone; ?>" class="inputbox" id="q_phone">
                                    </p>
                                </div>
                                <div class="cart-tab-block">
                                    <p><label for="q_postcode">Индекс:</label>
                                        <input type="text" name="q_postcode" value="<?php $q_postcode; ?>" class="inputbox" id="q_postcode">
                                    </p>
                                    <p><label for="q_city">Город:</label>
                                        <input type="text" name="q_city" value="<?php echo $q_city; ?>" id="q_city" class="inputbox" />
                                    </p>
                                    <p><label for="q_street">Улица:</label>
                                        <input type="text" name="q_street" value="<?php $q_street; ?>" class="inputbox" id="q_street">
                                    </p>
                                    <div class="b-input">
                                        <div class="col-b4 col-m4">
                                            <label for="q_haus">Дом:</label>
                                            <input type="text" name="q_haus" value="<?php $q_haus; ?>" class="inputbox" id="q_haus">
                                        </div>
                                        <div class="col-b4 col-m4">
                                            <label for="q_korp">Корпус:</label>
                                            <input type="text" name="q_korp" value="<?php $q_korp; ?>" class="inputbox" id="q_korp">
                                        </div>
                                        <div class="col-b4 col-m4">
                                            <label for="q_apart">Квартира:</label>
                                            <input type="text" name="q_apart" value="<?php $q_apart; ?>" class="inputbox" id="q_apart">
                                        </div>
                                    </div>
                                    <p><label for="q_comment">Дополнительная информация:</label>
                                        <textarea name="q_comment" id="q_comment" class="inputbox"><?php $q_comment; ?></textarea>
                                    </p>
                                    <input type="button" id="quicksubmit" class="btn btn-checkout btn-disabled" value="Отправить заказ" disabled="">
                                </div>
                            </form>
                        </div>

                        <!-- tab3 -->
                        <div role="tabpanel" class="tab-pane" id="order-login">
                            <form action="<?php echo $action2; ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="login" value="1">
                                <div class="cart-tab-block">
                                    <p class="checkout-info">Для авторизации введите ваш логин и пароль.</p>
                                    <p><label for="name">Логин / e-mail:</label>
                                        <input type="text" name="email" value="" class="inputbox" id="input-email">
                                    </p>
                                    <p><label for="name">Пароль:</label>
                                        <input type="password" name="password" value="" class="inputbox" id="input-password">
                                    </p>
                                    <a href="<?php echo $forgotten; ?>" class="password-link">Забыли пароль?</a>
                                </div>
                                <input type="button" value="Войти" id="button-login" class="btn" />
                            </form>
                        </div>
                        <?php } ?>
                    </div>
                </div>

            <!--<div class="user-btn">
                <a href="<?php echo $continue; ?>" class="back-in-cat back-in-cat__top btn">Продолжить покупки</a>
                <a href="<?php echo $checkout; ?>" class="btn btn-purple" style="float: right">Продолжить оформление</a>
            </div>-->
        </div>
    </div>
</div>

<script type="text/javascript"><!--
    var shipping_address = $('#collapse-shipping-method'),
            payment_method = $('#collapse-payment_method'),
            collapse_checkout_confirm = $('#collapse-checkout-confirm'),
            collapse_shipping_address = $('#collapse-shipping-address'),
            orderQuick = $('#order-fast');

    if (shipping_address.length) {
        shippingMethod();
    }

    if (payment_method.length) {
        paymentMethod();
    }

    if (collapse_shipping_address.length) {
        collapseShippingAddress();
    }

    if (collapse_checkout_confirm.length) {
        //collapseCheckoutConfirm();
    }

    var btc = $('.btn-checkout');

    $(document).ready(function () {
        $(document).delegate('#collapse-shipping-method input[name="shipping_method"]', 'click', function() {
	        buttonShippingMethod();
            collapseCheckoutConfirm();
            if ($(this).val() == 'multiflat.multiflat4') {
                $('#collapse-shipping-address').hide();
            } else {
                $('#collapse-shipping-address').show();
            }
        });

        $(document).delegate('#collapse-payment_method input[name="payment_method"]', 'click', function() {
           buttonPaymentMethod();
        });

        $(document).delegate('.tabs-nav a', 'click', function() {
            var href = $(this).attr('href');
            if (href == '#order-fast') {
                $('input[name="regist"]').val('2');
            } else if (href == '#order-reg') {
                $('input[name="regist"]').val('1');
            }
        });

        $( "#order-fast input" ).keyup(function() {

            if (orderQuick.find('input[name="q_name"]').val().length > 2 && orderQuick.find('input[name="q_phone"]').val().length > 2 && orderQuick.find('input[name="q_postcode"]').val().length > 2 && orderQuick.find('input[name="q_city"]').val().length > 2 && orderQuick.find('input[name="q_street"]').val().length > 2 && orderQuick.find('input[name="q_haus"]').val().length > 0) {
                $('#order-fast input[type="button"]').removeClass('btn-disabled').prop("disabled", false);
            } else {
                $('#order-fast input[type="button"]').addClass('btn-disabled').prop("disabled", true);
            }
        });

        $('#quicksubmit').on('click', function() {
            var formData = new Object();
            $('#order-fast input').each(function (i, val) {
                formData[val.name] = val.value;
            });

            formData['q_comment'] = $('#order-fast textarea').val();

            $.ajax({
                type: 'post',
                url: 'index.php?route=payment/cod/quick',
                cache: false,
                data: formData,
                beforeSend: function() {
                    //$('#button-confirm').button('loading');
                },
                complete: function() {
                    //$('#button-confirm').button('reset');
                },
                success: function(json) {
                    if (json.error) {
                        $('p.error').remove();
                        $.each(json.error, function(i, item) {
                            $('input[name="' + i + '"]').after('<p class="error">' + item + '</p>');
                        });
                    } else {
                        location = '/index.php?route=checkout/success';
                    }
                    //
                }
            });
        });
    });


    <?php if (!$logged) { ?>
        $('a.tablink').on('click', function() {
            var hrif = $(this).attr('href');
            if (hrif == '#order-login') {
                btc.hide();
            } else {
                btc.show();
            };
        });

        // Login
        $(document).delegate('#button-login', 'click', function() {
            $.ajax({
                url: 'index.php?route=checkout/login/save',
                type: 'post',
                data: $('#order-login :input'),
                dataType: 'json',
                beforeSend: function() {
                    $('.error-panel').remove();
                },
                complete: function() {
                    //$('#button-login').button('reset');
                },
                success: function(json) {
                    $('.alert, .text-danger').remove();
                    $('p').removeClass('has-error');

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#order-login').prepend('<div class="error-panel">' + json['error']['warning'] + '</div>');

                        // Highlight any found errors
                        $('#order-login input[name=\'email\']').parent().addClass('has-error');
                        $('#order-login input[name=\'password\']').parent().addClass('has-error');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    <?php } else { ?>

    <?php } ?>

    function shippingMethod() {
        $.ajax({
            url: 'index.php?route=checkout/shipping_method',
            dataType: 'html',
            success: function(html) {
                // Add the shipping address
                $.ajax({
                    url: 'index.php?route=checkout/shipping_address',
                    dataType: 'html',
                    success: function(html) {
                        $('#collapse-shipping-address .panel-body').html(html);

                        // $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });

                $('#collapse-shipping-method .panel-body').html(html);
                buttonShippingMethod();
                //$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                $('a[href=\'#collapse-shipping-method\']').trigger('click');
},
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function buttonShippingMethod() {
        $.ajax({
            url: 'index.php?route=checkout/shipping_method/save',
            type: 'post',
            data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
            dataType: 'json',
            beforeSend: function() {
                //$('#button-shipping-method').button('loading');
            },
            complete: function() {
                //$('#button-shipping-method').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    //location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function selectMethod() {
        console.log($('input:radio[name=shipping_method]:checked').parent().find('label').html());
    }

    function paymentMethod() {
        $.ajax({
            url: 'index.php?route=checkout/payment_method',
            dataType: 'html',
            success: function(html) {
                payment_method.html(html);

                $('a[href=\'#collapse-payment-method\']').trigger('click');
                $('#collapse-payment_method').removeClass('disabled');
                buttonPaymentMethod();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function buttonPaymentMethod() {
        $.ajax({
            url: 'index.php?route=checkout/payment_method/save',
            type: 'post',
            data: $('#collapse-payment_method input[type=\'radio\']:checked, #collapse-payment_method input[type=\'checkbox\']:checked, #collapse-payment_method textarea'),
            dataType: 'json',
            beforeSend: function() {
                //$('#button-payment-method').button('loading');
            },
            complete: function() {
                //$('#button-payment-method').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    //location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                }
                collapseCheckoutConfirm();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function collapseCheckoutConfirm() {
        $.ajax({
            url: 'index.php?route=checkout/confirm',
            dataType: 'html',
            success: function(html) {
                collapse_checkout_confirm.html(html);
                //$('a[href=\'#collapse-checkout-confirm\']').trigger('click');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function collapseShippingAddress () {
        $.ajax({
            url: 'index.php?route=checkout/shipping_address',
            cache: false,
            dataType: 'html',
            success: function(html) {
                collapse_shipping_address.html(html);

                $('a[href=\'#collapse-shipping-address\']').trigger('click');
                buttonShippingAddress()

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function buttonShippingAddress() {
        $.ajax({
            url: 'index.php?route=checkout/shipping_address/save',
            type: 'post',
            data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
            dataType: 'json',
            beforeSend: function() {
                //$('#button-shipping-address').button('loading');
            },
            complete: function() {
                //$('#button-shipping-address').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    //--></script>

<?php echo $footer; ?>