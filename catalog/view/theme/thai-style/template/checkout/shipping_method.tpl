<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
<p class="checkout-info">Выберите способ доставки</p>
<?php foreach ($shipping_methods as $shipping_method) { ?>
    <?php if (!$shipping_method['error']) { ?>
      <?php foreach ($shipping_method['quote'] as $quote) { ?>
      <table class="checkout-table">
      <tr>
        <td class="checkout-table__radio">
          <?php if ($quote['code'] == $code || !$code) { ?>
          <?php $code = $quote['code']; ?>
          <input class="checkbox-big" id="<?php echo $quote['code']; ?>" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" checked="checked" />
          <?php } else { ?>
          <input class="checkbox-big" id="<?php echo $quote['code']; ?>" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" />
          <?php } ?>
          <label for="<?php echo $quote['code']; ?>"><?php echo $quote['title']; ?></b></label>
        </td>
        <td class="checkout-table__label">
			<?php echo $quote['description']; ?>
		</td>
      </tr>
    </table>
      <?php } ?>
     
    <?php } else { ?>
    <div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
    <?php } ?>
<?php } ?>
<?php } ?>

<div class="buttons" style="display: none">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="btn btn-primary" />
  </div>
</div>
