<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">

        <div class="page-content search">
            <div class="search-head">
                <div class="page-title">Поиск</div>
                <div class="b-pagesearch">
                    <form action="/index.php" method="get">
                        <input type="text"  name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="inputbox">
                        <input type="hidden" name="route" value="product/search">
                        <input type="submit" value="Найти" id="button-search" class="btn" />
                    </form>
                </div>
            </div>
            <?php if (!empty($products)) { ?>
            <div class="search-result row">
                <div id="content"></div>
                    <?php foreach ($products as $product) { ?>
                    <div class="col-b3 col-m4 col-xs12">
                        <div class="product-item">
                            <a href="<?php echo $product['href']; ?>">
                                <?php if ($product['new']) { ?>
                                <span class="product-label new">NEW</span>
                                <?php } ?>
                                <?php if ($product['hit']) { ?>
                                <span class="product-label hit">ХИТ</span>
                                <?php } ?>
                                <?php if ($product['discounts']) { ?>
                                <span class="product-label sale">-<?php echo $product['discounts']; ?>%</span>
                                <?php } ?>
                                <div class="product-name"><?php echo (!empty($product['manufacturer']) ? $product['manufacturer'] . ' - ' : '') . $product['name'] . ' ' . $product['weight']; ?></div>
                                <div class="product-img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></div>
                                <?php if ($product['price']) { ?>
                                <div class="product-price"><?php echo $product['price']; ?> <i class="rub">д</i></div>
                                <?php } ?>
                            </a>
                            <!-- hover link -->
                            <div class="product-link prod-link-<?php echo $product['product_id']; ?>">
                                <?php if($product['item_wishlist']) { ?>
                                <a href="javascript:void(0);" class="popup-fav in-fav select" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"><span>В избранном</span></a>
                                <?php } else { ?>
                                <a href="javascript:void(0);" class="popup-fav in-fav" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span>В избранное</span></a>
                                <?php } ?>
                                <?php if($product['item_cart']) { ?>
                                <a href="javascript:void(0);" class="in-cart select" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span>Товар в корзине</span></a>
                                <?php } else { ?>
                                <a href="javascript:void(0);" class="in-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span>В корзину</span></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

            </div>
            <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <!--<div class="col-sm-6 text-right"><?php echo $results; ?></div>-->
            </div>
            <?php } else { ?>
            <p class="search-none">К сожалению ничего не найдено. Попробуйте ввести другой запрос.</p>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
/*$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});*/

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<script>
    $(window).load(function() {
        equalheight('.product-item');
        equalheight('.product-name');

        //slider
        var left1 = $('.ui-slider-handle').eq(0).position().left;
        $('.price-from').css('left', left1 - 17);

        var left2 = $('.ui-slider-handle').eq(1).position().left;
        $('.price-to').css('left', left2 - 17);

        var width = Math.max($(window).width(), window.innerWidth);
        if(width > 950) {
            $('#shop-filter').css('display', 'block');
        }else{
            $('#shop-filter').hide().css('opacity', 1);
        }
    });
</script>
<?php echo $footer; ?>