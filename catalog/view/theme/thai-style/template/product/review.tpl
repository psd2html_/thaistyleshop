<?php if ($reviews) { ?>
<div class="page_review_<?php echo $pagination_page; ?>"
<?php foreach ($reviews as $review) { ?>
<div class="review-item">
    <div class="review__head">
        <b>
            <?php if ($review['rating'] == 1) {
                echo 'Очень плохо :(';
            } elseif ($review['rating'] == 2) {
                echo 'Плохо.';
            } elseif ($review['rating'] == 3) {
                echo 'Так себе.';
            } elseif ($review['rating'] == 4) {
                echo 'Рекомендую всем гурманам.';
            } elseif ($review['rating'] == 5) {
                echo 'Отлично!';
            } ?>
        </b>  <span style="white-space: nowrap;"><?php echo $review['author']; ?>  (<?php echo $review['date_added']; ?>)</span>
        <div class="rate">
            <div class="stars-no-active" style="width:75px">
                <div class="stars-active" style="width:<?php echo $review['rating']*15 ?>px"></div>
            </div>
        </div>
    </div>
    <div class="review-body">
        <p><?php echo $review['text']; ?></p>
    </div>
</div>
<?php } ?>
<?php if ($pagination_next) { ?>
<div class="reviews__btn clearfix pagination">
    <a href="<?php echo $pagination_next; ?>" data-pagenext="<?php echo ($pagination_page + 1); ?>" class="btn">Показать ещё</a>
    <button id="add-review__open" class="btn btn-light">Оставить отзыв</button>
</div>
<?php } ?>
</div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
