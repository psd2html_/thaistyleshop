<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content productfull">
            <h1 class="productfull__title r-show"><?php echo $heading_title; ?> <span class="productfull__vol"><?php echo $weight; ?></span></h1>
            <?php if ($manufacturer) { ?>
            <a href="<?php echo $manufacturers; ?>" class="productfull__brand r-show"><?php echo $manufacturer; ?></a>
            <?php } ?>

            <div id="content" class="row">
                <?php if ($thumb || $images) { ?>
                    <?php if (!empty($images)) { ?>
                    <div class="productfull-img-wrap with-thumbs">
                        <div class="productfull-img">
                            <?php if ($discount) { ?>
                            	<span class="product-label sale">-<?php echo $discount; ?>%</span>
                            <?php } ?>
                            <i class="icon-zoom"></i>
                            <?php foreach ($images as $key=>$image) { ?>
                            <a href="<?php echo $image['popup']; ?>" class="img-zoom<?php echo ($key == 0 ? ' select' : ''); ?>" id="img<?php echo $key; ?>"><img src="<?php echo $image['big']; ?>" alt="<?php echo $heading_title; ?>"></a>
                            <?php } ?>
                        </div>
                        <div class="productfull-thumbs">
                            <?php foreach ($images as $key=>$image) { ?>
                            <span class="thumbs-link" data-image="img<?php echo $key; ?>"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>"></span>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="productfull-img-wrap with-thumbs">
                        <div class="productfull-img">
                        	<?php if ($discount) { ?>
                            	<span class="product-label sale">-<?php echo $discount; ?>%</span>
                            <?php } ?>
                            <a href="<?php echo $popup; ?>" class="img-zoom select" id="img<?php echo $key; ?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"></a>
                        </div>
                    </div>
                    <?php } ?>
                <?php } ?>

                <div id="product" class="productfull-info">
                    <h1 class="productfull__title r-hidden"><?php echo $heading_title; ?> <span class="productfull__vol"><?php echo $weight; ?></span></h1>
                    <?php if ($manufacturer) { ?>
                    <a href="<?php echo $manufacturers; ?>" class="productfull__brand r-hidden"><?php echo $manufacturer; ?></a>
                    <?php } ?>
                    <?php if (!empty($short_description)) { ?>
                    <div class="productfull__shortdesc r-hidden">
                        <p><?php echo $short_description; ?> </p>
                    </div>
                    <?php } ?>
                    <div class="clearfix">
                        <?php if ($price) { ?>
                        <div class="productfull__price clearfix">
                            <label>Цена</label>
                            <?php if ($special) { ?>
                            	<span class="productfull__price-num old">
                                	<b><?php echo $price; ?></b> <span class="rub">д</span>
								</span>
								<span class="productfull__price-num new">
	                                <b id="current-price"><?php echo $special; ?></b> <span class="rub">д</span>
	                            </span>
                            <?php } else { ?>
                            	<span class="productfull__price-num">
	                                <b id="current-price"><?php echo $price; ?></b> <span class="rub">д</span>
	                            </span>
                            <?php } ?>
                            
                            <span class="productfull__price-sale r-hidden">
                                <?php if ($discounts) { ?>
                                <?php foreach ($discounts as $discount) { ?>
                                <b id="sale-price" class="data-quantity" data-quantity="<?php echo $discount['quantity']; ?>"><?php echo round($discount['price'],2); ?> <span class="rub">д</span></b>  при покупке от <?php echo $discount['quantity']; ?> шт.
                                <?php } ?>
                                <?php } ?>
                            </span>
                        </div>
                        <div class="productfull__qty clearfix">
                            <label>Количество</label>
                            <div class="b-cart-qty">
                                <span class="qty-minus count">−</span>
                                <input type="text" name="quantity" id="quantity" class="inputbox current-qty" value="1">
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                <span class="qty-plus count">+</span>
                            </div>
                            <?php if ($discounts) { ?>
                            <?php foreach ($discounts as $discount) { ?>
                            <a class="productfull__qty-set r-hidden" href="#">Купить коробку (<?php echo $discount['quantity']; ?> шт.) -<?php echo (int)(100 - ($discount['price'] * 100 / str_replace(" ","",$price))) ?>%</a>
                            <?php } ?>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- btn-set and mob-price-4-box -->
                    <div class="productfull__btn">
	                    <? if ($quantity > 0) { ?>
                        <button type="button" id="button-cart"  class="btn btn-purple to-cart"><span>Добавить в корзину</span></button>
                        <? } else { ?>
                        <button type="button" id="button-cart" disabled="disabled"  class="btn btn-purple to-cart"><span>Временно нет в наличии</span></button>
                        <? } ?>

                        <div class="b-set r-show">
                            <?php if ($price) { ?>
                            <div>
                                <span class="old-price"><?php echo round($price,2); ?> <span class="rub">д</span></span>
                <span class="productfull__price-sale">
                    <?php if ($discounts) { ?>
                    <b id="sale-price"><?php echo round($discount['price'],2); ?> <span class="rub">д</span></b>  при покупке от <?php echo $discount['quantity']; ?> шт.
                    <?php } ?>
                </span>
                            </div>
                            <?php if ($discounts) { ?>
                            <a class="productfull__qty-set" href="#">Купить коробку (<?php echo $discount['quantity']; ?> шт.) -<?php echo (int)(100 - ($discount['price'] * 100 / $price)) ?>%</a>
                            <?php } ?>
                            <?php } ?>
                        </div>

                        <button type="button"  title="Добавить в избранное" class="btn btn-light in-fav" onclick="wishlist.add('<?php echo $product_id; ?>');"><span>Избранное</span></button>
                        <?php if (!empty($location)) { ?>
                        <a target="_blank" href="<?php echo $location; ?>" class="btn btn-light recipe"><span>Рецепт</span></a>
                        <?php } ?>
                    </div>
                    <!-- //btn-set and mob-price -->
                    </div>

                    <div class="productfull__share"></div>
                    
                    <!-- AddThis Button BEGIN -->
                    <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                    <div class="yashare-auto-init " data-yashareL10n="ru" data-yashareType="link" data-yashareQuickServices="vkontakte,facebook"></div>
                    <!-- AddThis Button END -->
                    
                </div>

            <div class="productfull-info-icon">
                <ul>
                    <li class="info__pay">Принимаем <span class="r-hidden">к оплате</span> наличные</li>
                    <li class="info__ship"><span class="r-hidden">Быстро доставим по Москве</span><span class="r-show">Быстрая доставка</span></li>
                    <li  class="info__sale"><span class="r-hidden">Предлагаем готовые наборы</span><span class="r-show">Дарим скидки</span></li>
                </ul>
            </div>
            <div class="row">
                <?php if (!empty($description)) { ?>
                <div class="col-b7 col-m6 col-s12">
                    <div class="productfull__description">
                        <h3>Описание</h3>
                        <?php echo $description; ?>
                    </div>
                </div>
                <?php } ?>
                <?php if ($attribute_groups) { ?>
                <div class="col-b5 col-m6 col-s12">
                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                    <div class="productfull__spec">
                        <h3><?php echo $attribute_group['name']; ?></h3>
                        <?php $end_element = array_pop($attribute_group['attribute']);
                        foreach ($attribute_group['attribute'] as $attribute) { ?>
                        <p><b><?php echo $attribute['name']; ?>:</b> <?php echo $attribute['text']; ?></p>
                        <?php } ?>
                        <p class="slife"><b><?php echo $end_element['name']; ?>:</b> <?php echo $end_element['text']; ?></p>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
            <!-- prod an -->
            <?php if ($products) { ?>
            <div class="productfull-related-carousel">
                <?php if ($manufacturer) { ?>
                <h3>Продукты бренда <?php echo $manufacturer; ?></h3>
                <?php } else { ?>
                <h3><?php echo $text_related; ?></h3>
                <?php } ?>
                <div class="productfull-related-carousel-holder">
                    <div id="brand-product">
                        <?php foreach ($products as $product) { ?>
                        <div class="col-xs12">
                            <div class="product-item">
                                <a href="<?php echo $product['href']; ?>">
                                    <?php if ($product['new']) { ?>
                                    <span class="product-label new">NEW</span>
                                    <?php } ?>
                                    <?php if ($product['hit']) { ?>
                                    <span class="product-label hit">ХИТ</span>
                                    <?php } ?>
                                    <?php if ($product['discounts']) { ?>
                                    <span class="product-label sale">-<?php echo $product['discounts']; ?>%</span>
                                    <?php } ?>
                                    <div class="product-name">
                                        <?php echo (!empty($product['manufacturer']) ? $product['manufacturer'] . ' - ' : '') . $product['name'] . ($product['width'] > 0 ? ' ' . $product['width'] . ' мл'  : ''); ?>
                                    </div>
                                    <div class="product-img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></div>
                                    <?php if ($product['price']) { ?>
                                    <div class="product-price"><?php echo $product['price']; ?> <span class="rub">д</span></div>
                                    <?php } ?>
                                </a>
                                <!-- hover link -->
                                <div class="product-link prod-link-<?php echo $product['product_id']; ?>">
                                    <?php if($product['item_wishlist']) { ?>
                                    <a href="javascript:void(0);" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');" class="in-fav select"><span>В избранном</span></a>
                                    <?php } else { ?>
                                    <a href="javascript:void(0);" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="in-fav"><span>В избранное</span></a>
                                    <?php } ?>
                                    <?php if($product['item_cart']) { ?>
                                    <a href="javascript:void(0);" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="in-cart select"><span>Товар в корзине</span></a>
                                    <?php } else { ?>
                                    <a href="javascript:void(0);" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="in-cart"><span>В корзину</span></a>
                                    <?php } ?>
                                 </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if ($review_status) { ?>
            <div class="row">
                <div class="col-b6 col-m6 col-s12">
                    <div class="productfull__reviews">
                        <h3>Отзывы покупателей (<?php echo $reviews_orig; ?>)</h3>
                        <div id="review"></div>
                        <div class="reviews__btn clearfix">
                            <button id="add-review__open" class="btn btn-light">Оставить отзыв</button>
                        </div>
                    </div>
                </div>
                <div class="col-b6 col-m6 col-s12">
                    <div class="productfull__add-reviews">
                        <h4>Добавить отзыв</h4>
                        <form>
                            <?php if ($review_guest) { ?>
                            <p>
                                <input type="text" class="inputbox" name="name" id="input-name" placeholder="Имя">
                            <div id="review_name"></div>
                            </p>
                            <p>
                                <textarea id="input-review" class="inputbox" name="text" placeholder="Текст отзыва"></textarea>
                                <div id="review_text"></div>
                                <!--<div class="help-block"><?php echo $text_note; ?></div>-->
                            </p>
                            <div class="add-reviews__btn clearfix">
                                <div class="rate">
                                    <label>Рейтинг</label>
                                    <div class="rate__stars">
                                        <div id="rate__blank"></div>
                                        <div id="rate__hover"></div>
                                        <div id="rate__votes"></div>
                                    </div>
                                    <input type="radio"  class="rate-star" name="rating" id="star1" value="1" style="display: none;"/>
                                    <input type="radio"  class="rate-star" name="rating" id="star2" value="2" style="display: none;" />
                                    <input type="radio"  class="rate-star" name="rating" id="star3" value="3" style="display: none;" />
                                    <input type="radio"  class="rate-star" name="rating" id="star4" value="4" style="display: none;" />
                                    <input type="radio"  class="rate-star" name="rating" id="star5" value="5" style="display: none;" />
                                    <div id="review_rating" style="margin-left: 15px; float: left"></div>
                                </div>
                                <button type="button" id="button-review"  data-loading-text="Идёт загрузка комментария" class="btn">Отправить отзыв</button>
                            </div>
                            <?php if ($site_key) { ?>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } else { ?>
                            <?php echo $text_login; ?>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        </div>
    </div>
</div>

<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			//$('#button-cart').button('loading');
		},
		complete: function() {
			//$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				//$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				//$('#cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);
				
                $('#cart').html(json['html']);
                $('#cart').find('.topmenu-dd').css('opacity','1').show();
                setTimeout(function(){
                    $('#cart').find('.topmenu-dd').css('opacity','0').hide();
                }, 3000);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

    //$('#review').fadeOut('slow');

    //$('#review').load(this.href);

    //$('#review').fadeIn('slow');
    $.ajax({
        url: this.href,
        type: 'get',
        dataType: 'html',
        beforeSend: function() {

        },
        complete: function() {
            $('.page_review_' + $(this).data('pagenext')).fadeIn('slow');
        },
        success: function(html) {
            $('.reviews__btn').remove();
            $('#review').after(html);
        }
    });

});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : ''),
		beforeSend: function() {
			//$('#button-review').button('loading');
            //$('#button-review').hide();
		},
		complete: function() {
			//$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
                //$('#button-review').show('slow');
                $.each(json['error'], function( index, element ) {
                    $('#review_' + index).html('<div class="error-panel">' + element + '</div>');
                });
			}

			if (json['success']) {
				$('.productfull__add-reviews form').html('<div class="success-panel">' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});

    width = Math.max($(window).width(), window.innerWidth);
    var margin_doc = $(".rate__stars").offset();
    if(width < 700){
        $('.productfull__add-reviews').hide();
    }

    $('.rate__stars').hover(function() {
                $('#rate__hover').show();
                $('#rate__votes').hide();
            },
            function() {
                $('#rate__votes').show();
                $('#rate__hover').hide();
            });

    $(".rate__stars").mousemove(function(e){
        var widht_votes = e.pageX - margin_doc.left;
        if (widht_votes == 0) widht_votes =1 ;
        user_votes = Math.ceil(widht_votes/15);
        $('#rate__hover').width(user_votes*15);
    });

    //rate fixing
    $('.rate__stars').click(function(){
        if(user_votes)
            $('#star'+user_votes).prop( "checked", true );
        $('#rate__votes').width(user_votes*15).show();
        $('#rate__hover').hide();

    });
});
//--></script>
<?php echo $footer; ?>