<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content catalog">
            <h1 class="page-title"><?php echo $heading_title; ?></h1>
            <div class="row">
                <?php echo $column_left; ?>
                <?php echo $column_filter; ?>
                <!-- pagination top -->
                <?php if (!empty($products)) { ?>
                <div class="pagination-top clearfix">
                    <?php if(!empty($_GET['limit']) && $_GET['limit'] == 1000) { ?>
                    <span class="pagination-list__title">ПОКАЗАТЬ ВСЁ</span>
                    <?php } ?>
                    <?php if(!empty($pagination)) { ?>
                    <a href="<?php echo $limits_all ?>" class="show-all-product">ПОКАЗАТЬ ВСЁ</a>
                    <ul class="pagination-list">
                        <span class="pagination-list__title">ПО СТРАНИЦАМ</span>
                        <?php echo $pagination; ?>
                    </ul>
                    <?php } elseif (!empty($_GET['limit']) && $_GET['limit'] == 1000) { ?>
                    <a href="<?php echo $limits_page ?>" class="show-all-product">ПО СТРАНИЦАМ</a>
                    <?php } ?>
                </div>
                <?php } ?>
                <!-- end.pagination top -->
                <!-- products list -->
                <div id="shop-products">
                    <?php if (!empty($products)) { ?>
                        <div class="row">
                            <?php foreach ($products as $product) { ?>
                            <div class="col-b4 col-m6 col-xs12">
                                <div class="product-item">
                                    <a href="<?php echo $product['href']; ?>">
                                        <?php if ($product['new']) { ?>
                                        <span class="product-label new">NEW</span>
                                        <?php } ?>
                                        <?php if ($product['hit']) { ?>
                                        <span class="product-label hit">ХИТ</span>
                                        <?php } ?>
                                        <?php if ($product['discounts']) { ?>
                                        <span class="product-label sale">-<?php echo $product['discounts']; ?>%</span>
                                        <?php } ?>
                                        <div class="product-name"><?php echo (!empty($product['manufacturer']) ? $product['manufacturer'] . ' - ' : '') . $product['name'] . ' ' . $product['weight']; ?></div>
                                        <div class="product-img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></div>
                                        <?php if ($product['special']) { ?>
                                        <div class="product-price"><div class="new-price"><?php echo $product['special']; ?> <i class="rub">д</i></div></div>
                                        <div class="product-price"><div class=""></div><div class="old-price"><?php echo $product['price']; ?> <i class="rub">д</i></div></div>
                                        <?php } elseif ($product['price']) { ?>
                                        <div class="product-price"><?php echo $product['price']; ?> <i class="rub">д</i></div>
                                        <?php } ?>
                                    </a>

                                    <!-- hover link -->
                                    <div class="product-link prod-link-<?php echo $product['product_id']; ?>">
                                        <?php if($product['item_wishlist']) { ?>
                                        <a href="javascript:void(0);" class="popup-fav in-fav select" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"><span>В избранном</span></a>
                                        <?php } else { ?>
                                        <a href="javascript:void(0);" class="popup-fav in-fav" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span>В избранное</span></a>
                                        <?php } ?>
                                        <?php if($product['item_cart']) { ?>
                                        <a href="javascript:void(0);" class="in-cart select" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span>Товар в корзине</span></a>
                                        <?php } else { ?>
                                        <a href="javascript:void(0);" class="in-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span>В корзину</span></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } else { ?>
                    <p>Продуктов нет</p>
                        <?php } ?>
                </div>
                <?php if (!empty($products) && !empty($pagination)) { ?>
                <!-- pagination bottom -->
                <div class="pagination-bottom clearfix">
                    <ul class="pagination-list">
                        <?php echo $pagination; ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <div class="shop-text description"><?php echo $content_bottom; ?></div>
        </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
