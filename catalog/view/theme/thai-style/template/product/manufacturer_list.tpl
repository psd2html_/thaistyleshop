<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>

        <h1 class="page-title"><?php echo $heading_title; ?></h1>
        <div class="page-content brands">
            <div id="content" class="row">
                <?php foreach ($categories as $category) { ?>
                <div class="col-b4 col-s6 col-xs-12 brand-item">
                    <div class="brand-item__img">
                        <?php if (!empty($category['image'])) { ?>
                        <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="brand"></a>
                        <?php } ?>
                    </div>
                    <div class="brand-item__desc">
                        <h4><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
                        <?php echo $category['description']; ?>
                    </div>
                </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>
<?php echo $footer; ?>