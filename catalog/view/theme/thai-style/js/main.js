/* Thanks to CSS Tricks for pointing out this bit of jQuery
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$.fn.equivalent = function (){
    var $blocks = $(this),
        maxH    = $blocks.eq(0).height(); 

    $blocks.each(function(){
        maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH;
    });
    return maxH;
}
var orderH = $('.order-info').equivalent();
$('.page-content').css('min-height', $('.text-content').height() -50 + orderH);

$(window).load(function() {
  equalheight('.brand-item');
  equalheight('.user-orders__td');
  
  //user order top
  var orderHeight = $('.user-orders__td').height();
  $('.order-info').css('top', orderHeight + 30);
  
});

$(document).ready(function () { 
  var width = Math.max($(window).width(), window.innerWidth); // different between queries and jQuery.width()
  
  //error hide
   $('.popup-content').find('.inputbox').on('focus', function(){
    $(this).parent('p').next('div').children('.error-panel').hide();
   });
  
  //scroll
  var miniCart = $('.minicart__table').height();
  if(miniCart > 215){
    $('.minicart__scroll').jScrollPane();
  }else{
    $('.minicart__scroll').css('overflow', 'auto');
  }
  $('.topmenu-dd').hide().css('opacity', 1);
  
  //check load and resize
  function check_window_size() {
    var telBox = $('.b-topmenu').find('.tel');
    width = Math.max($(window).width(), window.innerWidth);
    if (width > 1150 || width < 950){
      telBox.removeClass('dd-toggle');
      telBox.off('mouseenter');
      telBox.off('mouseleave');
    } else if(width <= 1150 && width > 950){
      telBox.addClass('dd-toggle');
      telBox.on({
        mouseenter: function () {
          $(this).find('.topmenu-dd').fadeIn(300);
          $(this).find('.shadow-help').fadeIn(400);
          $(this).next('li').addClass('next-li');
        },
        mouseleave: function () {
          $(this).find('.topmenu-dd').fadeOut(300);
          $(this).find('.shadow-help').fadeOut(200);
          $(this).next('li').removeClass('next-li');
        }
      });
    } 
    
    if(width > 950 || width < 700) {
      $('.mob-menu').removeClass('open-m-m');
      $('.b-topmenu-mob').hide();
    }

      // main submenu toggle
      if(width > 1050){
          $('.mainmenu-list li').on({
              mouseenter: function () {
                  $(this).find('ul').fadeIn(400);
              },
              mouseleave: function () {
                  $(this).find('ul').fadeOut(400);
              }
          });
      }else if(width <= 1050 && width > 700){
          $('.mainmenu-list li').off();
          $('.mainmenu-list li a').off();
          $('.mainmenu-list li a').on('click', function(e){
              e.preventDefault();
              $(this).toggleClass('open-sub-m');
              $(this).next('ul').fadeToggle(400);
          });
      }
    
    if(width > 700) {
      $('.mainmenu-list').css('display', 'block');
        $('.cat-menu').hide();
    }
    
    if(width > 700 && width < 950) {
      $('.productfull-img-wrap').width($('.productfull').width() - 400);
    }else if(width > 950) {
      $('.productfull-img-wrap').width($('.productfull').width() - 500);
    } else{
      $('.productfull-img-wrap').css('width', '100%');
    }

      //btn-up position
      if ($('#shop-filter').length) {
          if(width > 950){
              if($('#shop-filter').height() >= $('#shop-products').height()){
                  $('.btn-up').hide();
              }else{
                  jQuery(document).scroll(function () {
                      var top = $("#shop-filter").offset().top;
                      var totalTop = $("#shop-filter").offset().top + $('#shop-filter').height() - 30;
                      if (jQuery(this).scrollTop() > totalTop && jQuery(this).scrollTop() < $('#shop-products').height() + top - 30) {
                          jQuery('.btn-up').addClass('fixed');
                      } else {
                          jQuery('.btn-up').removeClass('fixed');
                      }
                  });
              }
          }
      }

      if ($(".mob-filter").length) {
          if(width > 700 && width <= 950){
              if($('.mob-filter').height() > $('#shop-products').height()){
                  $('.btn-up').hide();
              }else{
                  jQuery(document).scroll(function () {
                      var top = $(".mob-filter").offset().top;
                      var totalTop = $(".mob-filter").offset().top + $('.mob-filter').height() - 30;
                      if (jQuery(this).scrollTop() > totalTop && jQuery(this).scrollTop() < $('#shop-products').height() + top - 30) {
                          jQuery('.btn-up').addClass('fixed');
                      } else {
                          jQuery('.btn-up').removeClass('fixed');
                      }
                  });
              }
          }
      }

      //open search
      $('.b-search').find('a.btn-search').on('click', function(e){
          e.preventDefault();
          $(this).prev('form').slideToggle();
          $('.mainmenu-contact').toggle();
          setTimeout(function(){
              if(!$("#searchword").is(":focus")){
                  $('.b-search').find('form').hide();
                  $('.mainmenu-contact').toggle();
              }
          }, 3500);
      });

      //up
    /*var prodHeight = $('#shop-products').height();
    if(width > 950 && ((prodHeight - $('#shop-filter').height()) > 50 )){
      $('.btn-up').css('display', 'block');
      $('.btn-up').css('top', prodHeight - 90);
    }*/
    
  }// end check load and resize
  check_window_size();
  
  
  if(width <= 700) {
      $('.mainmenu-list').hide();
  }
  
  //header-hover  
  $('.b-topmenu').find('.dd-toggle').on({
    mouseenter: function () {
      $(this).find('.topmenu-dd').fadeIn(200);
      $(this).find('.shadow-help').fadeIn(200);
      $(this).next('li').addClass('next-li');
    },
    mouseleave: function () {
      $(this).find('.topmenu-dd').fadeOut(200);
      $(this).find('.shadow-help').fadeOut(100);
      $(this).next('li').removeClass('next-li');
    }
  });
  
  //up
  jQuery('.btn-up').click(function () {
      jQuery('body,html').animate({
        scrollTop: 0
      }, 500, 'easeOutQuad');
    });
  
  //open top mob-menu
  $('.mob-menu').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('open-m-m');
    $('.b-topmenu-mob').slideToggle();
  });

    // catmenu toggle mob
    $('#catmenu-toggle').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('open-cat-m');
        if($('.mob-menu-main').hasClass('open-main-m')){
            $('.mob-menu-main').toggleClass('open-main-m');
            $('.mainmenu-list').slideToggle();
        }
        $('.cat-menu').slideToggle();
    });

    //open main mob-menu
    $('.mob-menu-main').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('open-main-m');
        if($('#catmenu-toggle').hasClass('open-cat-m')){
            $('#catmenu-toggle').toggleClass('open-cat-m');
            $('.cat-menu').slideToggle();
        }
        $('.mainmenu-list').slideToggle();
    });
  
  //open add-review
  $('#add-review__open').on('click', function(e){
    $('.productfull__add-reviews').slideToggle();
  });
  
  //open left mob menu
  $('.user').find('.toogle-leftmenu-user').on('click', function(e){
    e.preventDefault();
    $('.leftside-nav').find('li').not('.current').slideToggle();
  });
  $('.toogle-leftmenu').on('click', function(e){
    e.preventDefault();
    $('.leftside-nav').slideToggle();
  });
  
  //open cart menu
  $('.tabs-nav').find('.active').parent('li').css('display', 'block');
  $('.cart-tabs').find('.toogle-cartmenu').on('click', function(e){
    e.preventDefault();
    $(this).hide();
    $('.tabs-nav').find('.tablink').not('.active').parent('li').slideToggle();
  });
  
  //user orders
  $('.user-orders').on('click', function(e){
    $(this).siblings('.user-orders').find('.order-info').hide();
    $(this).siblings('.user-orders').removeClass('order-open');
    $(this).toggleClass('order-open');
    $(this).find('.order-info').slideToggle(500);
  });
  
  //qty change  
  //in cart
  jQuery(".cart").find(".qty-minus").click(function(){
    var inputbox = jQuery(this).parent().children(".inputbox");
    var value = inputbox.val();
    if(value > 1){
        var res = parseInt(value) - 1;
    }else{
        var res = 1;
    }
    //inputbox.val(res);
      $('[name="' + inputbox.attr('name') + '"]').val(res);
  });
  jQuery(".cart").find(".qty-plus").click(function(){
      var inputbox = jQuery(this).parent().children(".inputbox");
      var value = inputbox.val();
      var res = parseInt(value) + 1;
      //inputbox.val(res);
      $('[name="' + inputbox.attr('name') + '"]').val(res);
  });
  
  //catalog-hover  
  $('.product-item').on({
    mouseenter: function () {
      $(this).children('.product-link').fadeIn(200);
    },
    mouseleave: function () {
      $(this).children('.product-link').fadeOut(200);
    }
  });
  
  //in prod
  var currentPrice = parseInt($('#current-price').text().replace(/\s+/g,""));
  var salePrice = parseInt($('#sale-price').text().replace(/\s+/g,""));
  var discountQuantity = parseInt($('.data-quantity').data('quantity'));
  jQuery(".productfull").find(".qty-minus").on('click', function(){
    var inputbox = jQuery(this).parent().children(".inputbox");
    var value = inputbox.val();
    if(value > 1){
        var res = parseInt(value) - 1;
    }else{
        var res = 1;
    }
    if(value <= discountQuantity){
      $('#current-price').text(parseFloat(currentPrice, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString());
    }
    inputbox.val(res);
  });
  jQuery(".productfull").find(".qty-plus").on('click', function(){
      var inputbox = jQuery(this).parent().children(".inputbox");
      var value = inputbox.val();
      var res = parseInt(value) + 1;
      if(value >= discountQuantity - 1){
        $('#current-price').text(parseFloat(salePrice, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString());
      }
      inputbox.val(res);
  });
  $('.productfull__qty-set').on('click', function(e){
    e.preventDefault();
    var salePrice = parseInt($('#sale-price').text().replace(/\s+/g,""));
    $('#current-price').text(parseFloat(salePrice, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString());
    
    var inputbox = $('.current-qty');
    var qty = inputbox.val();
    if((qty%discountQuantity) == 0){
      inputbox.val(parseInt(qty) + discountQuantity);
    }else{
      inputbox.val(discountQuantity);
    }
  });
  $('.productfull__qty .current-qty').on('change keyup', function(e){
  		var value = $(this).val();
        if(value >= discountQuantity - 1){
	        $('#current-price').text(parseFloat(salePrice, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString());
	      }
	     if(value < discountQuantity){
	      $('#current-price').text(parseFloat(currentPrice, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString());
	    }
  });
 
  //placeholder ie
  jQuery('[placeholder]').placeholder();

    /*$(".popup").click(function( e ) {
        e.preventDefault();
    });*/
    console.log("%c Остановитесь! ", "background: red; color: yellow; font-size: x-large");
    console.log("%c Эта функция браузера предназначена для разработчиков. Если кто-то сказал вам скопировать и вставить что-то здесь, это мошенники. Выполнив эти действия, вы несёте ответственность за взлом сайта. ", "color: grey; font-size: large");

  //popup
  $(".popup").fancybox({
    padding : 0,
		width		: '430px',
    height  : 'auto',
    autoSize: false,
    openSpeed : 500,
    closeSpeed : 500,
    overlay : {
      speedOut   : 250,   // duration of fadeOut animation
      showEarly  : false,  // indicates if should be opened immediately or wait until the content is ready
    },
    tpl: { 
        closeBtn: '<div title="Закрыть" class="fancybox-item fancybox-close">Закрыть</div>' 
          }
	});
  //prodfull popup
  $(".img-zoom").fancybox({
    padding : 30,
    overlay : {
      speedOut   : 250,   // duration of fadeOut animation
      showEarly  : false,  // indicates if should be opened immediately or wait until the content is ready
    },
    tpl: { 
        closeBtn: '<div title="Закрыть" class="fancybox-item fancybox-close">Закрыть</div>' 
          }
	});
  
  $(".popup-fav").fancybox({
    padding : 0,
		width		: '600px',
    height  : 'auto',
    autoSize: false,
    openSpeed : 500,
    closeSpeed : 500,
    openEffect : 'fade',
    overlay : {
      speedOut   : 250,   // duration of fadeOut animation
      showEarly  : false,  // indicates if should be opened immediately or wait until the content is ready
    },
    tpl: { 
        closeBtn: '<div title="Закрыть" class="fancybox-item fancybox-close">Закрыть</div>' 
          }
	});
  
  //zoom
  //$('.img-zoom').zoom();
  
  //owl
  $('#brand-product').owlCarousel({
    nav:true,
    margin:15,
    loop:true,
    responsive:{
        0:{
            items:1,
            margin:0
        },
        550:{
            items:2
        },
        800:{
            items:3
        },
        1000:{
            items:4,
            loop: false
        }
    }
  });

    //owl
    $('#brand-product').owlCarousel({
        nav:true,
        margin:15,
        loop:true,
        responsive:{
            0:{
                items:1,
                margin:0
            },
            550:{
                items:2
            },
            800:{
                items:3
            },
            1000:{
                items:4,
                loop: false
            }
        }
    });
  
  //thumb
    $('.thumbs-link').on('click', function(){
      var img = $(this).data('image');
      $('.img-zoom').hide();
      $('#'+img).css('display', 'block');
      console.log(img);
    });

    //back-btn user
    if($("div").is(".user-orders-table") || $("div").is("#user-favourite")){
        $('.user').find('.user-btn').css('top', '-70px');
    }else{
        $('.user').find('.user-btn').css('top', '0px');
    }
    
  
  //resize
  $(window).resize(function(e) {
    check_window_size();
    equalheight('.brand-item');
    equalheight('.user-orders__td');
    
    //user order top
    var orderHeight = $('.user-orders__td').height();
    $('.order-info').css('top', orderHeight + 30);
  });
});