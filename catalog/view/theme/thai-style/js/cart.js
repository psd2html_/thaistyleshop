/**
 * Created by AtrDevue on 04.07.15.
 */
$(document).ready(function () {
    width = Math.max($(window).width(), window.innerWidth);
    //tabs
    // insert slider

    if(width > 600){
        $('.tabs-nav').append('<div id="slider-hover"></div>');
        // initially reset
        var left = $('.tabs-nav li:first-child a').parent().position().left;
        var width = $('.tabs-nav li:first-child a').width();
        $('#slider-hover').css({'left' : left, 'width' : width+30+2});

        $(".tablink").on('click', function(e){
            e.preventDefault();
            $(".tablink").removeClass('active').css('color', '#888888;');
            $(this).addClass('active').css('color', '#fff;');
            var left = $(this).parent().position().left;
            var width = $(this).width();
            $(this).css('color', '#fff');

            $('#slider-hover').stop().animate({
                'left' : left,
                'width' : width+30+2
            });

            var tab = $(this).attr('href');
            $(".tab-pane").hide();
            $(tab).show();
        });
    }else{
        $('#slider-hover').remove();
        $(".tablink").on('click', function(e){
            e.preventDefault();
            $(".tablink").removeClass('active').css('color', '#888888;');
            $(this).addClass('active').css('color', '#fff;');
            $(this).css('color', '#fff');

            var tab = $(this).attr('href');
            $(".tab-pane").hide();
            $(tab).show();
        });
    }

    //slider hover
    /*function sliderHover() {
     width = Math.max($(window).width(), window.innerWidth);
     if(width < 600) {
     $('#slider-hover').remove();
     $('.tabs-nav li a').hover(function(){
     $(this).not('.active').css('color', '#888888');
     });
     }else if(width > 600 && !$("div").is("#slider-hover")){
     // insert slider
     $('.tabs-nav').append('<div id="slider-hover"></div>');
     // initially reset
     var left = $('.tabs-nav li:first-child a').parent().position().left;
     var width = $('.tabs-nav li:first-child a').width();
     $('#slider-hover').css({'left' : left, 'width' : width+30-6});
     // sliding
     $('.tabs-nav li a').hover(
     function(){
     var left = $(this).parent().position().left;
     var width = $(this).width();
     $(this).css('color', '#fff');

     $('#slider-hover').stop().animate({
     'left' : left,
     'width' : width+30+2
     });
     },function(){
     width = $('.tabs-nav li a.active').width();
     left = $('.tabs-nav li a.active').position().left;
     $(this).not('.active').css('color', '#888888');
     $('#slider-hover').stop().animate({
     'left' : left,
     'width' : width+30+2
     });
     });
     }else{
     width = $('.tabs-nav li a.active').width();
     left = $('.tabs-nav li a.active').parent().position().left;
     $('#slider-hover').stop().animate({
     'left' : left,
     'width' : width+30+2
     }, 300);
     }
     }
     sliderHover();*/

    //resize
    $(window).resize(function(e) {
        width = Math.max($(window).width(), window.innerWidth);
        if(width > 600){
            $('.tabs-nav').append('<div id="slider-hover"></div>');
            // initially reset
            var left = $('.tabs-nav .tablink.active').parent().position().left;
            var width = $('.tabs-nav .tablink.active').width();

            $('#slider-hover').css({'left' : left, 'width' : width+30+2});

            $(".tablink").on('click', function(e){
                e.preventDefault();
                $(".tablink").removeClass('active').css('color', '#888888;');
                $(this).addClass('active').css('color', '#fff;');
                var left = $(this).parent().position().left;
                var width = $(this).width();
                $(this).css('color', '#fff');

                $('#slider-hover').stop().animate({
                    'left' : left,
                    'width' : width+30+2
                });

                var tab = $(this).attr('href');
                $(".tab-pane").hide();
                $(tab).show();
            });
        }else{
            $('#slider-hover').remove();
            $(".tablink").on('click', function(e){
                e.preventDefault();
                $(".tablink").removeClass('active').css('color', '#888888;');
                $(this).addClass('active').css('color', '#fff;');
                $(this).css('color', '#fff');

                var tab = $(this).attr('href');
                $(".tab-pane").hide();
                $(tab).show();
            });
        }
    });

});