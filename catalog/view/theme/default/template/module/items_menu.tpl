<nav class="leftside">
    <a href="#" class="toogle-leftmenu"><?php echo $select_section; ?></a>
    <ul class="leftside-nav">
        <?php foreach ($informations as $information) { ?>
        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
        <?php } ?>
        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
    </ul>
</nav>