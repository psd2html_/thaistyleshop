<?php
class ModelTotalPersonal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		if ($this->config->get('personal_status')) {
			$this->load->language('total/personal');
					
			$discount = $this->customer->getDiscount();

			if ((float)$discount) {

				if ($discount > 0) {
					$total_data[] = array(
						'code'       => 'personal',
						'title'      => $this->language->get('text_personal') . $discount . '%',
						'value'      => ($total * ($discount/100)),
						'sort_order' => $this->config->get('personal_sort_order')
					);

					$total -= ($total * ($discount/100));
				}
			}
		
		}
	}

	public function confirm($order_info, $order_total) {
		$this->load->language('total/personal');

		if ($order_info['customer_id']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$order_info['customer_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', description = '" . $this->db->escape(sprintf($this->language->get('text_order_id'), (int)$order_info['order_id'])) . "', amount = '" . (float)$order_total['value'] . "', date_added = NOW()");
		}
	}

	public function unconfirm($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	}
}