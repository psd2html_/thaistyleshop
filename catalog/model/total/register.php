<?php
class ModelTotalRegister extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {

		$this->load->language('total/personal');
		
		$discount = $this->config->get('config_customer_first_discount');

		if ((float)$discount) {

			if ($discount > 0) {
				$total_data[] = array(
					'code'       => 'register',
					'title'      => 'Скидка за регистрацию ' . $discount . '%',
					'value'      => ($total * ($discount/100)),
					'sort_order' => $this->config->get('personal_sort_order')
				);

				$total -= ($total * ($discount/100));
			}
		}
	}

	public function confirm($order_info, $order_total) {
		$this->load->language('total/personal');

		if ($order_info['customer_id']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$order_info['customer_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', description = '" . $this->db->escape(sprintf($this->language->get('text_order_id'), (int)$order_info['order_id'])) . "', amount = '" . (float)$order_total['value'] . "', date_added = NOW()");
		}
	}

	public function unconfirm($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	}
}