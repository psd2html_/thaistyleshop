<?php
class ModelPaymentRbs extends Model {
    public function getMethod($address, $total) {
        $this->load->language('payment/rbs');

        $method_data = array(
            'code'     => 'rbs',
            'title'    => $this->language->get('text_title'),
            'terms'      => '',
            'sort_order' => 10
        );

        return $method_data;
    }
}