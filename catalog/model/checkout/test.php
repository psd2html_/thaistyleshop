<?php
/*
 * Тестовя задача: Написать программу, которая осуществляет поиск дубликатов файлов.
 * На вход подаётся путь, по которому должен осуществляться поиск.
 * На выходе - текстовый файл, в котором будут указаны все дубликаты, по 1 пути на строку.
 * Будет оценено затраченное время, качество кода, производительность программы, отказоустойчивость.
 * Пожалуйста, при выполнении замерте потраченное на выполнение время.
 */
/*
 * Ответ: в данной задаче небыло указанно предпологаемые массивы и объём данных.
 * Мы будим исходить из небольших объёмов данных файлов, в ином случае нужно писать совершено другую логику работы скриптов.
 *
 * Valentin rasulov <info@artdevue.com>
 */

echo 'Тест</br>';

$dir = "/var/www/test/test_img";
$file = "/var/www/test/test_result.txt";

$start_time = microtime(true);

// функция для сбора всех хеш файлов и полного пути к файлу в массив. Всё зависит от директорий. Если массив директории большие, то нужно другое решение
function GetListFiles($folder, &$all_files, &$all_name)
{
    $fp = opendir($folder);
    while ($cv_file = readdir($fp))
    {
        if (is_file($folder . "/" . $cv_file))
        {
            $all_files[] = $folder . "/" . $cv_file;
            $all_name[] = md5($cv_file);
        }
        elseif ($cv_file != "." && $cv_file != ".." && is_dir($folder . "/" . $cv_file))
        {
            GetListFiles($folder . "/" . $cv_file, $all_files, $all_name);
        }
    }
    closedir($fp);
}

// массив директорий
$all_files = [];
$all_name = [];

GetListFiles($dir, $all_files, $all_name);

$result = '';
// обходим массив с хешем, если значений больше чем одно, пишем в массив результатов
foreach ($all_name as $key => $value)
{
    if (count(array_keys($all_name, $value)) > 1)
    {
        $result .= $all_files[$key] . PHP_EOL;
    }
}

// записываем в файл
if (file_exists($file))
{
    unlink($file);
}
file_put_contents($file, $result, FILE_APPEND | LOCK_EX);

$end_time = microtime(true);

print 'Конец теста. <br/>Затраченно времени: ' . round(($end_time - $start_time), 5) . 'сек <br/>Памяти: ' . memory_get_usage(true) / (1024 * 1024) . " MB.";