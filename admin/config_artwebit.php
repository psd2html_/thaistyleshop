<?php
// HTTP
define('HTTP_SERVER', 'http://thaistyleshop.artwebit.ru/admin/');
define('HTTP_CATALOG', 'http://thaistyleshop.artwebit.ru/');

// HTTPS
define('HTTPS_SERVER', 'http://thaistyleshop.artwebit.ru/admin/');
define('HTTPS_CATALOG', 'http://thaistyleshop.artwebit.ru/');

// DIR
define('DIR_APPLICATION', $_SERVER['DOCUMENT_ROOT'].'/admin/');
define('DIR_SYSTEM', $_SERVER['DOCUMENT_ROOT'].'/system/');
define('DIR_LANGUAGE',$_SERVER['DOCUMENT_ROOT'].'/admin/language/');
define('DIR_TEMPLATE', $_SERVER['DOCUMENT_ROOT'].'/admin/view/template/');
define('DIR_CONFIG', $_SERVER['DOCUMENT_ROOT'].'/system/config/');
define('DIR_IMAGE', $_SERVER['DOCUMENT_ROOT'].'/image/');
define('DIR_CACHE', $_SERVER['DOCUMENT_ROOT'].'/system/storage/cache/');
define('DIR_DOWNLOAD', $_SERVER['DOCUMENT_ROOT'].'/system/storage/download/');
define('DIR_LOGS', $_SERVER['DOCUMENT_ROOT'].'/system/storage/logs/');
define('DIR_MODIFICATION', $_SERVER['DOCUMENT_ROOT'].'/system/storage/modification/');
define('DIR_UPLOAD', $_SERVER['DOCUMENT_ROOT'].'/system/storage/upload/');
define('DIR_CATALOG', $_SERVER['DOCUMENT_ROOT'].'/catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'thai_user');
define('DB_PASSWORD', 'nqP5EDIZWX');
define('DB_DATABASE', 'thai_db');
define('DB_PREFIX', 'ts_');
