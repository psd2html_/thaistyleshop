<?php

/**
 * Created by PhpStorm.
 * User: AtrDevue - common.php
 * Date: 02.04.15
 * Time: 21:42
 * Project: thai-style
 */
class ModelModuleThaiFull extends Model
{
    public $table_name = 'thai_full';

    public function checkInstall()
    {

        $sql = " SHOW TABLES LIKE '" . DB_PREFIX . $this->table_name . "'";
        $query = $this->db->query($sql);
        if (count($query->rows) <= 0)
        {
            $this->createTable();
        }

    }

    public function checkUninstall()
    {
        $sql = " SHOW TABLES LIKE '" . DB_PREFIX . $this->table_name . "'";
        $query = $this->db->query($sql);
        if (count($query->rows) > 0)
        {
            $query = ' DROP TABLE ' . DB_PREFIX . $this->table_name;
            $this->db->query($query);
        }
    }

    public function getListData()
    {
        $query = ' SELECT * FROM ' . DB_PREFIX . $this->table_name;

        $query = $this->db->query($query);
        $row = $query->rows;

        $out = array();
        if (!empty($row))
        {
            foreach ($row as $r)
            {
                $out[$r['alias']] = $r['value'];
            }
        }

        return $out;
    }

    public function editModule($data)
    {
        foreach( $data as $key => $value )
        {
            $this->db->query("UPDATE `" . DB_PREFIX . $this->table_name . "` SET `value` = '" . $this->db->escape($value) . "' WHERE `alias` = '" . $key . "'");
        }

        return true;
    }

    protected function createTable()
    {
        $q = "
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . $this->table_name . "` (
				   	  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `alias` varchar(255) NOT NULL,
					  `group_id` int(11) NOT NULL DEFAULT 0,
					  `value` text NOT NULL,
					  PRIMARY KEY (`id`),
					  INDEX(`alias`),
					  INDEX(`group_id`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

		";
        $this->db->query($q);
    }
}