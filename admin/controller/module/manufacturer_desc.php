<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - manufacturer_desc.php
 * Date: 05.04.15
 * Time: 12:44
 * Project: thai-style
 */
class ControllerModuleManufacturerDesc extends Controller
{
    private $error = array();

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('module/manufacturer_desc');
    }

    public function index()
    {
        $this->load->language('module/manufacturer_desc');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('manufacturer_desc', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_status'] = $this->language->get('entry_status');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/manufacturer_desc', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['action'] = $this->url->link('module/manufacturer_desc', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['manufacturer_desc_status'])) {
            $data['manufacturer_desc_status'] = $this->request->post['manufacturer_desc_status'];
        } else {
            $data['manufacturer_desc_status'] = $this->config->get('manufacturer_desc_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/manufacturer_desc.tpl', $data));

    }

    public function install()
    {
        $this->model_module_manufacturer_desc->checkInstall();
    }

    public function uninstall()
    {
        $this->model_module_manufacturer_desc->checkUninstall();
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'module/manufacturer_desc')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}